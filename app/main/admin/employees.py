from flask import redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_admin.model.form import InlineFormAdmin
from flask_admin.form import rules
from flask_login import current_user
from ... import admin, db
from ...models import Employee, HondaCertification as HC,\
    ContactNumber as CN, DesignatedDriver as DD,\
    DriverHondaCertification


from datetime import date
from flask_admin.model import typefmt


def date_format(view, value):
    return value.strftime('%b %d, %Y')


def none_formatter(view, value):
    return "FOR UPDATE"


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    date: date_format,
    type(None): none_formatter,
})


hc_remarks = [
    (u'PASSED/CAN DRIVE SAFELY', u'PASSED/CAN DRIVE SAFELY'),
    (u'PASSED/CAN DRIVE SAFELY(M/T)', u'PASSED/CAN DRIVE SAFELY(M/T)'),
    (u'PASSED/CAN DRIVE SAFELY(A/T)', u'PASSED/CAN DRIVE SAFELY(A/T)'),
    (u'PASSED BUT NO LATEST RECORD', u'PASSED BUT NO LATEST RECORD'),
    (u'FAILED/CANNOT DRIVE SAFELY', u'FAILED/CANNOT DRIVE SAFELY'),
    (u'FAILED EXAM/NO PRACTICAL', u'FAILED EXAM/NO PRACTICAL'),
    (u'NO RECORD/FOR CHECKING', u'NO RECORD/FOR CHECKING'),
    (u'NO LICENSE', u'NO LICENSE'),
    (u'SCHEDULED', u'SCHEDULED'),
]


class HCInlineModelForm(InlineFormAdmin):
    form_choices = {
        'remarks': hc_remarks
    }

    form_widget_args = dict(
        date=dict(type='date')
    )

    column_labels = dict(mt='M/T', at='A/T')


class DDHCInlineModelForm(InlineFormAdmin):
    form_choices = {
        'remarks': hc_remarks
    }

    form_widget_args = dict(
        date=dict(type='date')
    )

    column_labels = dict(mt='M/T', at='A/T')


class DDInlineForm(InlineFormAdmin):
    form_columns = ('id', 'name', 'relation', 'license_no',
                    'notes', )
    inline_models = [DDHCInlineModelForm(DriverHondaCertification)]


class EmployeeView(ModelView):
    can_export = True
    can_view_details = True
    page_size = 50
    details_template = 'admin/employee/details.html'
    inline_models = (CN, HCInlineModelForm(HC), DDInlineForm(DD), )
    column_sortable_list = (('name', 'last_name'))
    column_default_sort = 'last_name'
    column_list = [
        'name', 'entity', 'employee_no', 'email',
        'SBU', 'BU', 'department', 'section', 'position', 'location',
        'doh', 'gender', 'license_no', 'license_type', 'classification',
        'classification_code', 'honda_certification', 'waiver',
    ]
    column_filters = [
        'last_name', 'first_name', 'entity', 'employee_no', 'email', 'doh',
        'SBU', 'BU', 'department', 'position', 'location',
        'license_no', 'license_type', 'classification_code', 'gender',
        'honda_certifications', 'waiver',
    ]
    column_export_list = [
        'name', 'entity', 'employee_no', 'email',
        'SBU', 'BU', 'department', 'section', 'position', 'location',
        'doh', 'gender', 'license_no', 'license_type', 'classification',
        'classification_code', 'honda_certifications', 'waiver',
        'designated_drivers', 'tot',
    ]
    form_rules = [
        rules.FieldSet(
            ('first_name', 'middle_name', 'last_name', 'email',
             'gender', 'contact_numbers', ),
            'Basic Information'
        ),
        rules.FieldSet(
            ('employee_no', 'entity', 'SBU', 'BU', 'department', 'section',
             'cost_center', 'classification_code', 'classification',
             'location', 'position', 'doh', 'is_sales', ), 'Employment Information'
        ),
        rules.FieldSet(
            ('license_no', 'date_issued', 'license_expiry',
             'license_type', 'waiver', ),
            'License Information'
        ),
        rules.FieldSet(
            ('designated_drivers', 'honda_certifications',
             'tot', 'remarks'),
            'Miscellaneous Information'
        )
    ]

    column_labels = dict(email='Email Address',
                         SBU='SBU',
                         BU='BU',
                         tot='Tool of Trade / Fringe Benefit (For Finance)',
                         doh='DOH',
                         is_sales='Sales?',
                         waiver='Has Waiver?')

    form_widget_args = dict(
        doh=dict(type='date'),
        date_issued=dict(type='date'),
        license_expiry=dict(type='date')
    )

    form_choices = {
        'gender': [
            (u'Male', u'Male'),
            (u'Female', u'Female'),
        ],
        'license_type': [
            (u'pro', 'Professional'),
            (u'non-pro', 'Non-Professional'),
        ]
    }

    column_type_formatters = MY_DEFAULT_FORMATTERS

    def get_details_columns(self):
        return [
            ('name', 'Name: '),
            ('employee_no', 'Employee #: '),
            ('email', 'Email Address: '),
            ('license_no', 'License #: '),
            ('date_issued', 'Date Issued: '),
            ('license_expiry', 'Expires on: '),
            ('honda_certifications', 'Honda Certification Record: '),
            ('SBU', 'SBU: '),
            ('BU', 'BU: '),
            ('cost_center', 'Cost Center: '),
            ('classification', 'Classification: '),
            ('department', 'Department: '),
            ('location', 'Location: '),
            ('position', 'Position: '),
            ('tot', 'Tool of Trade / Fringe Benefit: ')
        ]

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class HondaCertificationView(ModelView):
    can_export = True
    form_choices = {
        'remarks': hc_remarks
    }

    column_list = [
        'employee', 'date', 'total_score',
        'mt', 'at', 'remarks',
    ]

    form_widget_args = dict(
        date=dict(type='date'),
    )

    column_filters = ['employee.last_name', 'remarks']

    column_type_formatters = MY_DEFAULT_FORMATTERS

    column_labels = dict(mt='M/T', at='A/T')

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class DDHondaCertificationView(ModelView):
    can_export = True
    form_choices = {
        'remarks': hc_remarks
    }

    column_list = [
        'employee', 'date', 'total_score',
        'mt', 'at', 'remarks',
    ]

    form_widget_args = dict(
        date=dict(type='date'),
    )

    column_filters = ['employee.last_name', 'remarks']

    column_type_formatters = MY_DEFAULT_FORMATTERS

    column_labels = dict(mt='M/T', at='A/T')

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class DesignatedDriverView(ModelView):
    can_export = True
    column_filters = ['name', 'employee', ]

    form_columns = ('employee', 'name', 'relation',
                    'license_no', 'notes', )

    inline_models = [DDHCInlineModelForm(DriverHondaCertification)]

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


admin.add_view(EmployeeView(Employee, db.session,
                            name='Employees',
                            endpoint='employees',
                            category='Employees'))

admin.add_view(HondaCertificationView(HC, db.session,
                                      name='Honda Certifications',
                                      endpoint='honda-certifications',
                                      category='Employees'))

admin.add_view(DesignatedDriverView(
    DD, db.session, name='Designated Drivers',
    endpoint='designated-drivers',
    category='Employees'))
