from flask import redirect, request, url_for
from flask_login import current_user

from flask_admin.contrib.sqla import ModelView
from flask_admin.model import typefmt
from flask_admin.contrib.sqla.view import func
from flask_admin.contrib.sqla.ajax import QueryAjaxModelLoader
from flask_admin.model.form import InlineFormAdmin
from flask_admin.form import rules
from wtforms import validators
from ... import admin, db

from ...models import Vehicle, LeasingInformation, Dealer, Employee,\
    VehiclesToEmployees, Insurance

from datetime import date
from flask_admin.model import typefmt


def date_format(view, value):
    try:
        return value.strftime('%b %d, %Y')
    except ValueError:
        return "FOR UPDATE"


def string_format(view, value):
    return "FOR UPDATE"


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    date: date_format,
    type(None): string_format,
})


FUEL_TYPES = [
    (u'Gas', u'Gas'),
    (u'Diesel', u'Diesel'),
]

STATUS_TYPES = [
    (u'ACTIVE', u'ACTIVE'),
    (u'INCOMING', u'INCOMING'),
    (u'FOR BIDDING/DISPOSAL', u'FOR BIDDING/DISPOSAL'),
    (u'UNASSIGNED', u'UNASSIGNED'),
    (u'DISPOSED/SOLD', u'DISPOSED/SOLD')
]

LEASING_COMPANIES = (
    (u'BPI Rental', u'BPI Rental'),
    (u'CCAC Owned', u'CCAC Owned'),
)
CONTRACT_TYPES = (
    (u'OPERATING LEASE', u'OPERATING LEASE'),
    (u'OWNED', u'OWNED'),
)


class LIIinlineModelForm(InlineFormAdmin):

    form_widget_args = {
        'contract_start': {
            'type': 'date',
        },
        'contract_end': {
            'type': 'date',
        }
    }

    column_descriptions = {
        'contract_start': """
        <p>This will be the basis for the following notifications:</p>
        <ul>
          <li>First PM (Notification to be sent 2 weeks after this)</li>
          <li>Second and beyond PM (Notification to be sent on specified monthly interval, check <a target="_blank" href="%s">PM Schedule</a>)</li>
          <li>LTO Registration (Notification for the first 3 years and annualy after that. <strong>depends on Plate No</strong></li>
          <li>Battery Replacement (Notification after this date + 2 years)</li>
        </ul>
        """ % "/admin/pm-schedule/",
        'contract_end': """
        <p>This will be the basis for the following notifications:</p>
        <ul>
          <li><strong>IF Primary Lease</strong> Primary Lease Expiration (3 months before)</li>
          <li><strong>IF Secondary Lease</strong> Secondary Lease Expiration (3 months before)</li>
        </ul>
        """
    }

    column_labels = dict(
        is_primary='Is Primary Lease?')


class InsuranceInlineModelForm(InlineFormAdmin):
    pass


class AssignmentModelForm(InlineFormAdmin):
    form_choices = {
        'tot_fringe': [
            (u'Tool of Trade', u'Tool of Trade'),
            (u'Fringe Benefit', u'Fringe Benefit'),
        ]
    }

    form_widget_args = dict(
        assigned_start=dict(type='date'),
        assigned_end=dict(type='date'),
    )

    form_columns = [
        'id',
        'employee',
        'assigned_start',
        'assigned_end',
        'is_primary',
        'tot_fringe',
    ]

    def on_model_change(self, form, model, is_created):
        pass
        # try:
        #     if model.employee.assignment_eligible:
        #         pass
        #     else:
        #         raise validators.ValidationError(
        #         'Employee is not Honda Certified / License is not PRO / No waiver')
        # except AttributeError:
        #     pass

    column_labels = dict(
        tot_fringe='Tool of Trade / Fringe Benefit (FOR DISPOSAL)')


class DealerView(ModelView):
    can_export = True
    column_filters = ('name', )

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class VehicleView(ModelView):
    can_view_details = True
    details_template = 'admin/vehicle/details.html'
    list_template = 'admin/vehicle/list.html'
    can_export = True
    column_list = [
        'primary_assignee.name', 'plate_no', 'cs_no', 'sticker_no',
        'engine_no', 'year', 'make_model', 'fuel', 'color',
        'lto_mv_file_no', 'chassis_no', 'status', 'date_disposed',
        'oac_policy_no', 'effectivity_period_start', 'effectivity_period_end',
        'ctpl_insurance', 'comprehensive_insurance',
        'leasing_company', 'contract_type',
        'acquisition_cost', 'dealer', 'shell_fleet_card', 'credit_limit',
        'with_rfid', 'remarks',
        # Lease Information
        'primary_lease_account_number', 'primary_lease_contract_start',
        'primary_lease_contract_end', 'primary_lease_term',
        'primary_lease_monthly_rental',
        'secondary_lease_account_number', 'secondary_lease_contract_start',
        'secondary_lease_contract_end', 'secondary_lease_term',
        'secondary_lease_monthly_rental',
    ]

    column_default_sort = 'year'

    column_filters = ('assignees.last_name', 'assignees.SBU',
                      'plate_no', 'cs_no', 'engine_no',
                      'sticker_no', 'year', 'make_model',
                      'lto_mv_file_no', 'leasing_company',
                      'status', 'shell_fleet_card', 'date_disposed')

    column_labels = {
        'primary_assignee.name': 'Assignee',
        'cs_no': 'CS #',
        'plate_no': 'Plate #',
        'sticker_no': 'Sticker #',
        'engine_no': 'Engine #',
        'make_model': 'Make/Model',
        'lto_mv_file_no': 'LTO MV File #',
        'oac_policy_no': 'OAC Policy #',
        'fuel_type': 'Fuel Type',
        'status_string': 'Status',
        'leasing_company_string': 'Leasing Company',
        'contract_type_string': 'Contract Type'
    }
    inline_models = (
        AssignmentModelForm(VehiclesToEmployees),
        LIIinlineModelForm(LeasingInformation),
        InsuranceInlineModelForm(Insurance),
    )
    column_sortable_list = ('acquisition_cost', 'year')
    form_rules = [
        rules.FieldSet(
            ('assignments',),
            'Assignees Information'
        ),
        rules.FieldSet(
            ('plate_no', 'cs_no', 'sticker_no',
             'year', 'make_model', 'fuel',
             'engine_no', 'chassis_no', 'color',
             'lto_mv_file_no', 'status', 'date_disposed',
             'dealer', ),
            'Vehicle Information'
        ),
        rules.FieldSet(
            ('insurance_record', ),
            'Insurance Information',
        ),
        rules.FieldSet(
            ('leasing_company', 'contract_type',
             'acquisition_cost', 'leasing_information'),
            'Lease Information'
        ),
        rules.FieldSet(
            ('shell_fleet_card', 'credit_limit',
             'with_rfid', 'remarks'),
            'Miscellaneous Information'
        )
    ]

    page_size = 500

    column_formatters = dict(
        acquisition_cost=lambda v, c, m, p: "{:,}".format(
            int(m.acquisition_cost)
        ) if m.acquisition_cost is not None else "FOR UPDATE",
        credit_limit=lambda v, c, m, p: "{:,}".format(
            int(m.credit_limit)
        ) if m.credit_limit is not None else "FOR UPDATE",
        primary_lease_monthly_rental=lambda v, c, m, p: "{:,}".format(
            m.primary_lease_monthly_rental
        ) if m.primary_lease_monthly_rental is not None else "FOR UPDATE",
        secondary_lease_monthly_rental=lambda v, c, m, p: "{:,}".format(
            float(m.secondary_lease_monthly_rental)
        ) if m.secondary_lease_monthly_rental is not None else "FOR UPDATE",
        ctpl_insurance=lambda v, c, m, p: "{:,}".format(
            float(m.ctpl_insurance)
        ) if m.ctpl_insurance is not None else "FOR UPDATE",
        comprehensive_insurance=lambda v, c, m, p: "{:,}".format(
            float(m.comprehensive_insurance)
        ) if m.comprehensive_insurance is not None else "FOR UPDATE",
        remarks = lambda v, c, m, p: m.remarks if m.remarks != "" else "FOR UPDATE",
        secondary_lease_term = lambda v, c, m, p: m.secondary_lease_term if m.secondary_lease_term is not None else "FOR UPDATE"
        # secondary_lease_monthly_rental=lambda v, c, m, p: "{:,}".format(
        #     m.secondary_lease_monthly_rental
        # ) if m.secondary_lease_monthly_rental is not None else ""
    )

    column_type_formatters = MY_DEFAULT_FORMATTERS

    form_widget_args = dict(
        date_assigned=dict(type='date'),
        effectivity_period_start=dict(type='date'),
        effectivity_period_end=dict(type='date'),
    )

    form_choices = {
        'fuel': FUEL_TYPES,
        'status': STATUS_TYPES,
        'leasing_company': LEASING_COMPANIES,
        'contract_type': CONTRACT_TYPES,
    }

    form_ajax_refs = {
        'assignees': QueryAjaxModelLoader(
            'assignees',
            db.session,
            Employee,
            fields=['last_name', 'first_name', 'middle_name'],
            placeholder='Search Employees'
        ),
        'dealer': QueryAjaxModelLoader(
            'dealer',
            db.session,
            Dealer,
            fields=['name', 'city', 'address'],
            placeholder='Search Dealers'
        )
    }

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))

    def get_query(self):
        return self.session.query(self.model).filter(
            self.model.status != 'DISPOSED/SOLD').filter(
                self.model.status != 'INCOMING'
            )

    def get_count_query(self):
        return self.session.query(func.count('*')).filter(
            self.model.status != 'DISPOSED/SOLD').filter(
                self.model.status != 'INCOMING'
            )


class DisposedVehicleView(VehicleView):
    can_create = False

    def get_query(self):
        return self.session.query(self.model).filter(
            self.model.status == 'DISPOSED/SOLD')

    def get_count_query(self):
        return self.session.query(func.count('*')).filter(
            self.model.status == 'DISPOSED/SOLD')


class IncomingVehicleView(VehicleView):
    can_create = False

    def get_query(self):
        return self.session.query(self.model).filter(
            self.model.status == 'INCOMING'
        )

    def get_count_query(self):
        return self.session.query(func.count('*')).filter(
            self.model.status == 'INCOMING'
        )


admin.add_view(VehicleView(Vehicle, db.session,
                           name='Vehicles',
                           endpoint='vehicles',
                           category='Vehicles'))

admin.add_view(DisposedVehicleView(Vehicle, db.session,
                                   name='Disposed Vehicles',
                                   endpoint='disposed-vehicles',
                                   category='Vehicles'))


admin.add_view(IncomingVehicleView(Vehicle, db.session,
                                   name='Incoming Vehicles',
                                   endpoint='incoming-vehicles',
                                   category='Vehicles'))

admin.add_view(DealerView(Dealer, db.session,
                          name='Dealers',
                          endpoint='dealers',
                          category='Vehicles'))
