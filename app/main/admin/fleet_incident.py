from flask import redirect, request, url_for

from datetime import date

from flask_admin.contrib.sqla import ModelView
from flask_admin.model import typefmt
from flask_login import current_user
from ... import admin, db

from ...models import Incident, AccidentNature


def date_format(view, value):
    try:
        return value.strftime('%b %d, %Y')
    except ValueError:
        return ""


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    date: date_format,
})


class AccidentNatureView(ModelView):
    form_columns = ['name', ]

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class FleetIncidentView(ModelView):
    can_view_details = True
    details_template = 'admin/incident/details.html'
    can_export = True
    form_columns = [
        'assignment',
        'designated_driver',
        'date',
        'location',
        'accident_nature',
        'details',
        'damaged_portion',
        'is_claimable',
        'repair_shop',
        'loa',
        'repair_cost',
        'remarks',
    ]
    form_widget_args = dict(
        date=dict(type='date')
    )
    column_filters = [
        'fleet_incident_id',
        'accident_nature',
        'date',
        'location',
        'assignment.employee.last_name',
        'assignment.vehicle.plate_no',
        'assignment.vehicle.cs_no',
    ]

    column_list = [
        'fleet_incident_id', 'assignment.vehicle', 'assignment.employee',
        'designated_driver', 'date', 'location', 'accident_nature',
        'details', 'damaged_portion', 'is_claimable', 'repair_shop', 'loa',
        'repair_cost', 'remarks',
    ]

    column_labels = {
        'fleet_incident_id': 'Fleet Incident ID',
        'assignment.vehicle': 'Vehicle',
        'assignment.employee': 'Employee',
        'date': 'Date of Incident',
        'location': 'Location of Incident',
        'accident_nature': 'Accident Classification',
        'details': 'Incident Details',
        'loa': 'LOA',
        'repair_cost': 'Insurance Repair Cost',
        'is_claimable': 'Claimable?',
        'assignment.employee.last_name': 'Employee\'s Last Name',
        'assignment.vehicle.plate_no': 'Plate #',
        'assignment.vehicle.cs_no': 'CS #',
    }

    form_choices = {
        'is_claimable': [
            (u'True', u'Claimable'),
            (u'False', u'Not Claimable'),
        ]
    }

    column_type_formatters = MY_DEFAULT_FORMATTERS

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))

    def after_model_change(self, form, model, is_created):
        if is_created:
            year = model.date.year
            last_record = db.session.query(Incident).filter(
                Incident.fleet_incident_id.like(str(year) + '%'))\
                .order_by(Incident.fleet_incident_id.desc())\
                .first()
            try:
                last_id = last_record.fleet_incident_id
                last_digits = int(last_id[-3:])
                to_save = '{:03d}'.format(last_digits + 1)
                model.fleet_incident_id = str(year) + '-V-' + to_save
                
            except AttributeError:
                model.fleet_incident_id = str(date.today().year) + '-V-001'


admin.add_view(FleetIncidentView(Incident, db.session, name='Incidents',
                                 endpoint='incidences',
                                 category='Fleet Incidents Monitoring'))
admin.add_view(AccidentNatureView(AccidentNature, db.session,
                                  name='Accident Classification',
                                  endpoint='accident-classifications',
                                  category='Fleet Incidents Monitoring'))
