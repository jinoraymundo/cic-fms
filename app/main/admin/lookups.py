from flask import redirect, request, url_for
from flask_login import current_user

from flask_admin.contrib.sqla import ModelView
from ... import admin, db
from ...models import EmployeeEntity as Entity,\
    EmployeeDepartment as Department,\
    EmployeeSection as Section,\
    EmployeeClassification as Classification,\
    EmployeeClassificationOther as ClassificationOther,\
    EmployeeSBU as SBU,\
    EmployeeBU as BU,\
    EmployeeLocation as Location,\
    EmployeePosition as Position,\
    EmployeeTOT as TOT,\
    EmployeeCostCenter as CostCenter,\
    InsuranceType


class GenericModelView(ModelView):
    column_filters = ['name', ]
    category = 'Lookups'

    def __init__(self, *args, **kwargs):
        super(GenericModelView, self).__init__(*args, **kwargs)
        self.category = 'Lookups'

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


# Lookups
admin.add_view(GenericModelView(Entity, db.session,
                                name='Entities',
                                endpoint='entities'))

admin.add_view(GenericModelView(Department, db.session,
                                name='Departments',
                                endpoint='departments'))

admin.add_view(GenericModelView(Section, db.session,
                                name='Sections',
                                endpoint='sections'))

admin.add_view(GenericModelView(Classification, db.session,
                                name='Classifications Codes',
                                endpoint='classification_codes'))

admin.add_view(GenericModelView(ClassificationOther, db.session,
                                name='Classifications',
                                endpoint='classifications'))

admin.add_view(GenericModelView(SBU, db.session, name='SBUs',
                                endpoint='sbus'))

admin.add_view(GenericModelView(BU, db.session, name='BUs', endpoint='bus'))

admin.add_view(GenericModelView(CostCenter, db.session, name='Cost Centers',
                                endpoint='cost-centers'))

admin.add_view(GenericModelView(Location, db.session, name='Locations',
                                endpoint='locations'))

admin.add_view(GenericModelView(Position, db.session, name='Positions',
                                endpoint='positions'))

admin.add_view(GenericModelView(TOT, db.session, name='TOT/Fringe Benefit',
                                endpoint='tool_of_trade_fringe_benefits'))

admin.add_view(GenericModelView(InsuranceType, db.session, name='Insurance Types',
                                endpoint='insurance-types'))
