from datetime import date

from flask_admin.contrib.sqla import ModelView
from flask_admin.model import typefmt
from flask_admin.contrib.sqla.view import func

from ... import admin, db
from ...models import Vehicle


def date_format(view, value):
    try:
        return value.strftime('%b %d, %Y')
    except ValueError:
        return ""


def string_format(view, value):
    return "FOR UPDATE"


def my_list_format(view, value):
    if value:
        return value
    return "FOR UPDATE"


def my_boolean_format(view, value):
    return value


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    date: date_format,
    type(None): string_format,
    list: my_list_format,
    bool: my_boolean_format,
})


class MasterListView(ModelView):
    can_create = False
    can_edit = False
    can_delete = False
    can_export = True
    column_display_actions = False
    column_sortable_list = []

    export_types = ['csv', 'xlsx', ]

    column_type_formatters = MY_DEFAULT_FORMATTERS
    column_type_formatters_export = MY_DEFAULT_FORMATTERS

    column_list = [
        'cs_no',
        'plate_no',
        'sticker_no',
        'year',
        'make_model',
        'fuel',
        'engine_no',
        'chassis_no',
        'color',
        'lto_mv_file_no',
        'primary_assignee.first_name',
        'primary_assignee.middle_name',
        'primary_assignee.last_name',
        'primary_assignee.name',
        'primary_assignee.classification_code.name',
        'primary_assignee.tot.name',
        'primary_assignment.tot_fringe',
        'primary_assignee.sales_display',
        'primary_assignee.BU.name',
        'primary_assignee.SBU.name',
        'primary_assignee.cost_center.name',
        'primary_assignee.entity.name',
        'primary_assignee.classification.name',
        'primary_assignee.department.name',
        'primary_assignee.section.name',
        'primary_assignee.position.name',
        'primary_assignee.designated_drivers',
        'dealer.name',
        'primary_assignee.honda_certification',
        'status',
        'primary_assignee.location.name',
        'primary_assignment.assigned_start',
        'ctpl_insurance_policyno',
        'ctpl_insurance',
        'ctpl_insurance_effectivity_period',
        'comprehensive_insurance_policyno',
        'comprehensive_insurance',
        'comprehensive_insurance_effectivity_period',
        'leasing_company',
        'contract_type',
        'acquisition_cost',
        'primary_lease_account_number',
        'primary_lease_contract_start',
        'primary_lease_contract_end',
        'primary_lease_term',
        'primary_lease_monthly_rental',
        'secondary_lease_account_number',
        'secondary_lease_contract_start',
        'secondary_lease_contract_end',
        'secondary_lease_term',
        'secondary_lease_monthly_rental',
        'shell_fleet_card',
        'credit_limit',
        'with_rfid',
    ]

    column_labels = {
        'cs_no': 'CS #',
        'plage_no': 'Plate #',
        'lto_mv_file_no': 'LTO MV File No.',
        'primary_assignee.first_name': 'First Name',
        'primary_assignee.middle_name': 'Middle Name',
        'primary_assignee.last_name': 'Last Name',
        'primary_assignee.name': 'Full Name',
        'primary_assignee.classification_code.name': 'Classification',
        'primary_assignee.tot.name': 'Fringe Benefit / Tool of Trade (For Finance)',
        'primary_assignment.tot_fringe': 'Fringe Benefit / Tool of Trade (For Disposal)',
        'primary_assignee.sales_display': 'Is sales?',
        'primary_assignee.BU.name': 'BU',
        'primary_assignee.SBU.name': 'SBU',
        'primary_assignee.cost_center.name': 'Cost Center',
        'primary_assignee.entity.name': 'Entity',
        'primary_assignee.classification.name': 'Other Classification',
        'primary_assignee.department.name': 'Department',
        'primary_assignee.section.name': 'Section',
        'primary_assignee.position.name': 'Position',
        'primary_assignee.designated_drivers': 'Designated Drivers',
        'dealer.name': 'Dealer',
        'primary_assignee.honda_certification': 'Honda Certification',
        'status': 'Vehicle Status',
        'primary_assignee.location.name': 'Location',
        'primary_assignment.assigned_start': 'Date Assigned',
        'ctpl_insurance_policyno': 'CTPL Insurance Policy No',
        'ctpl_insurance': 'CTPL Insurance',
        'ctpl_insurance_effectivity_period': 'CTPL Insurance Effectivity Period',
        'comprehensive_insurance_policyno': 'Comprehensive Insurance Policy No',
        'comprehensive_insurance': 'Comprehensive Insurance',
        'comprehensive_insurance_effectivity_period': 'Comprehensive Insurance Effectivity Period',
        'primary_lease_account_number': 'Primary Lease Account #',
        'primary_lease_contract_start': 'Contract Start',
        'primary_lease_contract_end': 'End Date',
        'primary_lease_term': 'Lease Term',
        'primary_lease_monthly_rental': 'Monthly Rental - Primary Lease Term',
        'secondary_lease_account_number': 'Secondary Lease Account #',
        'secondary_lease_contract_start': 'Contract Start',
        'secondary_lease_contract_end': 'End Date',
        'secondary_lease_term': 'Lease Term',
        'secondary_lease_monthly_rental': 'Monthly Rental - Secondary Lease Term',
        'shell_fleet_card': 'Shell Fleet Card No.',
        'with_rfid': 'With RFID'
    }

    def get_query(self):
        return self.session.query(self.model).filter(
            self.model.status!='DISPOSED/SOLD').filter(
                self.model.status!='INCOMING'
            )

    def get_count_query(self):
        return self.session.query(func.count('*')).filter(
            self.model.status!='DISPOSED/SOLD').filter(
                self.model.status!='INCOMING'
            )

    column_formatters = dict(
        acquisition_cost=lambda v, c, m, p: "{:,}".format(
            int(m.acquisition_cost)) if m.acquisition_cost is not None else "FOR UPDATE",
        credit_limit=lambda v, c, m, p: "{:,}".format(
            int(m.credit_limit)) if m.credit_limit is not None else "FOR UPDATE",
        primary_lease_monthly_rental=lambda v, c, m, p: "{:,}".format(
            m.primary_lease_monthly_rental) if m.primary_lease_monthly_rental is not None else "FOR UPDATE",
        secondary_lease_monthly_rental=lambda v, c, m, p: "{:,}".format(
            m.secondary_lease_monthly_rental) if m.secondary_lease_monthly_rental is not None else "FOR UPDATE",
        ctpl_insurance=lambda v, c, m, p: "{:,}".format(
            float(m.ctpl_insurance)) if m.ctpl_insurance is not None else "FOR UPDATE",
        comprehensive_insurance=lambda v, c, m, p: "{:,}".format(
            float(m.comprehensive_insurance)) if m.comprehensive_insurance is not None else "FOR UPDATE",
        remarks = lambda v, c, m, p: m.remarks if m.remarks != "" else "FOR UPDATE",
        secondary_lease_term = lambda v, c, m, p: m.secondary_lease_term if m.secondary_lease_term is not None else "FOR UPDATE",
    )

    column_filters = ('assignees.SBU', 'assignees.cost_center', )

    page_size = 1000


admin.add_view(MasterListView(Vehicle, db.session,
                              name='Master List',
                              endpoint='master-list'))
