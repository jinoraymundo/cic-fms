from flask import redirect, request, url_for
from flask_login import current_user

from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.ajax import QueryAjaxModelLoader
from flask_admin.model import typefmt
from ... import admin, db

from datetime import date
from ...models import RepairMaintenance, Employee, Dealer, Vehicle,\
    RepairType, PMSchedule


def date_format(view, value):
    try:
        return value.strftime('%b %d, %Y')
    except ValueError:
        return ""


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    date: date_format,
})


class RandMView(ModelView):
    can_export = True
    column_filters = ('employee.last_name',
                      'employee.SBU',
                      'employee.cost_center',
                      'date_requested',
                      'activity_concern',
                      'vehicle.plate_no', 'vehicle.cs_no', 'repair_types',
                      'invoice_no', )
    can_view_details = True
    column_editable_list = [
        'date_requested',
        'repair_types',
        'cost_estimate',
        'actual_amount',
        'activity_concern',
        'invoice_no',
        'km_reading',
    ]

    details_template = 'admin/repairs_maintenance/details.html'
    form_widget_args = dict(
        date_requested=dict(type='date'),
        date_serviced=dict(type='date'),
        date_forwarded=dict(type='date'),
        date_received=dict(type='date'),
    )

    column_default_sort = ('date_requested', True)

    column_list = [
        'vehicle',
        'employee',
        'employee.SBU',
        'employee.cost_center',
        'date_requested',
        'repair_types',
        'activity_concern',
        'cost_estimate',
        'actual_amount',
        'dealer',
        'invoice_no',
        'km_reading',
        'remarks',
    ]

    form_columns = [
        'vehicle',
        'employee',
        'repair_types',
        'date_requested',
        'activity_concern',
        'cost_estimate',
        'dealer',
        'actual_amount',
        'invoice_no',
        'km_reading',
        'remarks',
    ]

    form_ajax_refs = {
        'employee': QueryAjaxModelLoader(
            'employee',
            db.session,
            Employee,
            fields=['last_name', 'first_name', 'middle_name'],
            placeholder='Search Employees'
        ),
        'dealer': QueryAjaxModelLoader(
            'dealer',
            db.session,
            Dealer,
            fields=['name', 'city', 'address'],
            placeholder='Search Dealers'
        ),
        'vehicle': QueryAjaxModelLoader(
            'vehicle',
            db.session,
            Vehicle,
            fields=['make_model', 'plate_no', 'cs_no', ],
            placeholder='Search Vehicles'
        )
    }

    column_labels = {
        'employee': 'Requested by',
        'km_reading': 'Odometer Reading',
        'activity_concern': 'Activity/Concern',
        'date_requested': 'Date Serviced',
        'employee.last_name': 'Employee\'s Last Name',
        'vehicle.plate_no': 'Plate #',
        'vehicle.cs_no': 'CS #',
        'employee.SBU': 'SBU',
        'employee.cost_center': 'Cost Center',
    }

    column_type_formatters = MY_DEFAULT_FORMATTERS

    column_formatters = dict(
        cost_estimate=lambda v, c, m, p: "{:,.2f}".format(
            float(m.cost_estimate)) if m.cost_estimate is not None else "",
        actual_amount=lambda v, c, m, p: "{:,.2f}".format(
            float(m.actual_amount)) if m.actual_amount is not None else "")

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class PMScheduleView(ModelView):
    form_columns = [
        'unit', 'first_pm', 'second_pm',
        'interval'
    ]

    column_labels = dict(
        unit='Unit',
        first_pm='First PM',
        second_pm='Second PM',
        interval='Interval (Months)'
    )

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class RepairTypeView(ModelView):
    form_columns = ('name', )


admin.add_view(RandMView(RepairMaintenance, db.session,
                         name='Repairs and Maintenance',
                         endpoint='repairs-maintenance',
                         category='R&M'))

admin.add_view(RepairTypeView(RepairType, db.session,
                         name='Repair Types',
                         endpoint='repair-types',
                         category='R&M'))


admin.add_view(PMScheduleView(PMSchedule, db.session,
                              name='PM Schedule',
                              endpoint='pm-schedule',
                              category='R&M'))
