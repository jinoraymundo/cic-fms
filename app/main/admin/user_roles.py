from flask_admin.contrib.sqla import ModelView
from ... import admin, db
from ...models import User, Role

admin.add_view(ModelView(User, db.session, endpoint='users',
                         category='Admin'))

admin.add_view(ModelView(Role, db.session, endpoint='roles',
                         category='Admin'))
