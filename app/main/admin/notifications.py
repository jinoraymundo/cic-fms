from flask import redirect, request, url_for
from flask import current_app as app
from flask_login import current_user
from datetime import date
from wtforms.ext.dateutil.fields import DateTimeField

import os.path as op
import requests
import calendar
import collections

from flask import url_for
from flask_admin import BaseView, expose, form
from flask_admin.contrib.sqla import ModelView
from flask_admin.model.form import InlineFormAdmin
from wtforms.widgets import TextArea
from wtforms.fields import TextAreaField
from dateutil.relativedelta import relativedelta

from jinja2 import Markup

from ...models import EmailTemplate, EmailNotification, EmailTemplateAttachment
from ...models import Vehicle
from ...models import NotificationLog

from ... import admin, db
from ... import config

import pytz


ytd = date.today().year
mtd = date.today().month
months = [x for x in calendar.month_name]
months_dict = collections.OrderedDict({v: [] for k, v in enumerate(months)})


file_path = op.join(
    op.dirname(
        op.dirname(op.dirname(__file__))), 'static', 'notification_images')


attachment_path = op.join(
    op.dirname(
        op.dirname(op.dirname(__file__))), 'static', 'notification_attachments')


def get_first_pm(vehicles):
    first_pm_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        try:
            if vehicle.notifications['first_pm_due'].year == ytd:
                first_pm_months[calendar.month_name[vehicle.notifications['first_pm_due'].month]].append(vehicle)
        except KeyError:
            pass
    return first_pm_months


def get_month_first_pm(vehicles):
    first_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.notifications['first_pm_due'].year == ytd and \
                    vehicle.notifications['first_pm_due'].month == mtd:
                first_pms.append(vehicle)
        except KeyError:
            pass
    return sorted(first_pms, key=lambda vehicle: vehicle.notifications['first_pm_due'])


def get_second_pm(vehicles):
    second_pm_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        try:
            if vehicle.notifications['second_pm_due']:
                if vehicle.notifications['second_pm_due'].year == ytd:
                    second_pm_months[calendar.month_name[vehicle.notifications['second_pm_due'].month]].append(vehicle)
        except KeyError:
            pass
    return second_pm_months


def get_succeeding_pm(vehicles, ordinal='first_pm'):
    succeeding_pms_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        try:
            if vehicle.pms:
                if vehicle.pms[ordinal]:
                    if vehicle.pms[ordinal].year == ytd:
                        succeeding_pms_months[calendar.month_name[vehicle.pms[ordinal].month]].append(vehicle)
        except KeyError:
            pass

    for item in succeeding_pms_months['January']:
        print item

    for month in succeeding_pms_months:
        succeeding_pms_months[month] = sorted(succeeding_pms_months[month], key=lambda t: t.pms[ordinal])

    return succeeding_pms_months



def get_month_second_pm(vehicles):
    second_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.notifications['second_pm_due']:
                if vehicle.notifications['second_pm_due'].year == ytd and \
                        vehicle.notifications['second_pm_due'].month == mtd:
                    second_pms.append(vehicle)
        except KeyError:
            pass
    return sorted(second_pms, key=lambda vehicle: vehicle.notifications['second_pm_due'])


def get_primary_lease_expiration(vehicles):
    ple_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        if vehicle.primary_lease_contract_end:
            lease_notification = vehicle.notifications['primary_lease_expiration']
            if lease_notification.year == ytd:
                ple_months[calendar.month_name[lease_notification.month]].append(vehicle)
    return ple_months


def get_month_primary_lease_expiration(vehicles):
    ple = []
    for vehicle in vehicles:
        if vehicle.primary_lease_contract_end:
            lease_notification = vehicle.notifications['primary_lease_expiration']
            if lease_notification.year == ytd and lease_notification.month == mtd:
                ple.append(vehicle)
    return sorted(ple, key=lambda vehicle: vehicle.notifications['primary_lease_expiration'])


def get_secondary_lease_expiration(vehicles):
    sle_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        if vehicle.secondary_lease_contract_end:
            lease_notification = vehicle.notifications['secondary_lease_expiration']
            if lease_notification.year == ytd:
                sle_months[calendar.month_name[lease_notification.month]].append(vehicle)
    return sle_months


def get_month_secondary_lease_expiration(vehicles):
    sle = []
    for vehicle in vehicles:
        if vehicle.secondary_lease_contract_end:
            lease_notification = vehicle.notifications['secondary_lease_expiration']
            if lease_notification.year == ytd and lease_notification.month == mtd:
                sle.append(vehicle)
    return sorted(sle, key=lambda vehicle: vehicle.notifications['secondary_lease_expiration'])


def get_battery_replacement(vehicles):
    br_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        try:
            if vehicle.notifications['battery'].year == ytd:
                br_months[calendar.month_name[vehicle.notifications['battery'].month]].append(vehicle)
        except KeyError:
            pass
    return br_months


def get_month_battery_replacement(vehicles):
    battery = []
    for vehicle in vehicles:
        try:
            if vehicle.notifications['battery'].year == ytd and \
                    vehicle.notifications['battery'].month == mtd:
                battery.append(vehicle)
        except:
            pass
    return sorted(battery, key=lambda vehicle: vehicle.notifications['battery'])


def get_lto_reg(vehicles):
    lto_months = collections.OrderedDict({v: [] for k, v in enumerate(months)})
    for vehicle in vehicles:
        try:
            if vehicle.notifications['lto_reg'].year == ytd:
                lto_months[calendar.month_name[vehicle.notifications['lto_reg'].month]].append(vehicle)
        except KeyError:
            pass
    return lto_months


def get_month_lto_reg(vehicles):
    lto = []
    for vehicle in vehicles:
        try:
            if vehicle.notifications['lto_reg'].year == ytd and \
                    vehicle.notifications['lto_reg'].month == mtd:
                lto.append(vehicle)
            if vehicle.notifications['lto_reg'].year < ytd and \
                    vehicle.notifications['lto_reg'].month == mtd:
                lto.append(vehicle)
        except:
            pass
    return sorted(lto, key=lambda vehicle: vehicle.notifications['lto_reg'])


class EmailView(ModelView):

    def on_model_change(self, form, model, is_created):
            html = ""
            poster_path = op.join(
                op.dirname(op.dirname(op.dirname(__file__))),
                'static',
                'notification_images',
                model.email_template.poster,
            )
            # attachment_path = op.join(
            #     op.dirname(op.dirname(op.dirname(__file__))),
            #     'static',
            #     'notification_attachments',
            #     model.email_template.email_template_attachments,
            # )

            attachment_path = op.join(
                op.dirname(op.dirname(op.dirname(__file__))),
                'static',
                'notification_attachments',
            )

            if model.email_template.poster:
                html += Markup('<img src="cid:%s">' % model.email_template.poster)
            if model.email_template.template:
                html += Markup("%s" % model.email_template.template)

            data = {
                "from": app.config['MAIL_FROM'],
                # change this later
                "to": [model.recipient.email, ],
                "subject": model.email_template.name,
                "html": html,
            }

            # files = [
            #     ("inline", open(poster_path)),
            # ]

            files = []

            if model.email_template.email_template_attachments:
                for attachment in model.email_template.email_template_attachments:
                    files.append(
                        ("attachment", open(op.join(attachment_path, attachment.attachment)))
                    )

            requests.post(
                app.config['MAILGUN_URL'],
                auth=("api", app.config['MAILGUN_API_KEY']),
                files=files,
                data=data
            )


class CKEditorWidget(TextArea):
    def __call__(self, field, **kwargs):
        if kwargs.get('class'):
            kwargs['class'] += " ckeditor"
        else:
            kwargs.setdefault('class', 'ckeditor')
        return super(CKEditorWidget, self).__call__(field, **kwargs)


class CKEditorField(TextAreaField):
    widget = CKEditorWidget()


class EmailTemplateAttachmentInlineModelForm(InlineFormAdmin):

    form_columns = ('id', 'attachment', )

    form_overrides = dict(
        attachment=form.FileUploadField,
    )

    form_args = {
        'attachment': {
            'label': 'Attachment',
            'base_path': attachment_path,
            'allow_overwrite': True,
        }
    }


class EmailTemplateView(ModelView):
    extra_js = ['//cdn.ckeditor.com/4.6.2/full/ckeditor.js']
    form_overrides = dict(
        poster=form.FileUploadField,
        # attachment=form.FileUploadField,
        template=CKEditorField,
    )

    form_args = {
        'poster': {
            'label': 'Poster',
            'base_path': file_path,
            'allow_overwrite': True,
        },
        # 'attachment': {
        #     'label': 'Attachment',
        #     'base_path': attachment_path,
        #     'allow_overwrite': True,
        # }
    }

    inline_models = (
        EmailTemplateAttachmentInlineModelForm(EmailTemplateAttachment),
    )

    form_columns = [
        'name', 'poster', 'template',
    ]

    def on_model_change(self, form, model, is_created):
        print form
        return

    def _list_thumbnail(view, context, model, name):
        if not model.poster:
            return ''
        return Markup('<img width="500" src="%s">' % url_for(
            'static', filename=op.join('notification_images',
                                       model.poster)))

    def _list_template(view, context, model, name):
        if not model.template:
            return ''
        return Markup(model.template)

    column_formatters = {
        'poster': _list_thumbnail,
        'template': _list_template,
    }

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class ThisYearNotification(BaseView):
    @expose('/')
    def index(self):
        vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
        return self.render('notifications/this_year.html',
                            license={},
                            lto=get_lto_reg(vehicles),
                            battery=get_battery_replacement(vehicles),
                            primary_lease=get_primary_lease_expiration(vehicles),
                            secondary_lease=get_secondary_lease_expiration(vehicles),
                            months_sorted=months)


class ThisYearNotificationPMS(BaseView):
    @expose('/')
    def index(self):
        vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
        return self.render('notifications/this_year_pms.html',
                            first_pm=get_first_pm(vehicles),
                            succeeding_pm_first=get_succeeding_pm(vehicles),
                            succeeding_pm_second=get_succeeding_pm(vehicles, 'second_pm'),
                            succeeding_pm_third=get_succeeding_pm(vehicles, 'third_pm'),
                            months_sorted=months)


class ThisMonthNotification(BaseView):
    @expose('/')
    def index(self):
        vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
        return self.render('notifications/this_month.html',
                           first_pms=get_month_first_pm(vehicles),
                           second_pms=get_month_second_pm(vehicles),
                           lto_reg=get_month_lto_reg(vehicles),
                           battery=get_month_battery_replacement(vehicles),
                           primary_lease=get_month_primary_lease_expiration(vehicles),
                           secondary_lease=get_month_secondary_lease_expiration(vehicles),
                           ytd=ytd)


class NotificationLogView(ModelView):
    can_export = True
    column_filters = ('recipient', 'notification_type')
    column_formatters = dict(time_sent=lambda v,c,m,p: m.time_sent.replace(tzinfo=pytz.utc).astimezone(tz=pytz.timezone('Asia/Manila')))

    def get_query(self):
        return super(NotificationLogView, self).get_query().\
            order_by(NotificationLog.time_sent.desc())

    def get_count_query(self):
        return super(NotificationLogView, self).get_count_query()        


class FirstPMView(EmailView):
    def get_query(self):
        first_pm_email = EmailTemplate.query.filter_by(name='First Periodic Maintenance Reminder').one()
        return super(FirstPMView, self).get_query().filter_by(
            email_template=first_pm_email)


class SecondPMView(EmailView):
    def get_query(self):
        second_pm_email = EmailTemplate.query.filter_by(name='Periodic Maintenance Reminder').one()
        return super(SecondPMView, self).get_query().filter_by(
            email_template=second_pm_email)


class BatteryReplacementView(EmailView):
    def get_query(self):
        battery_replacement_email = EmailTemplate.query.filter_by(
            name='Battery Replacement').one()
        return super(BatteryReplacementView, self).get_query().filter_by(
            email_template=battery_replacement_email)


class LicenseRenewalView(EmailView):
    def get_query(self):
        license_renewal_email = EmailTemplate.query.filter_by(
            name='License Renewal').one()
        return super(LicenseRenewalView, self).get_query().filter_by(
            email_template=license_renewal_email)


class LTORegistrationView(EmailView):
    def get_query(self):
        lto_reg = EmailTemplate.query.filter_by(name='LTO Registration').one()
        return super(LTORegistrationView, self).get_query().filter_by(
            email_template=lto_reg)


class PrimaryLeaseExpirationView(EmailView):
    def get_query(self):
        template = EmailTemplate.query.filter_by(
            name='Lease Expiration - Primary').one()
        return super(PrimaryLeaseExpirationView, self).get_query().filter_by(
            email_template=template)


class SecondaryLeaseExpirationView(EmailView):
    def get_query(self):
        template = EmailTemplate.query.filter_by(
            name='Lease Expiration - Secondary').one()
        return super(SecondaryLeaseExpirationView, self).get_query().filter_by(
            email_template=template)


admin.add_view(EmailTemplateView(EmailTemplate, db.session,
                                 name='Email Templates',
                                 endpoint='email-templates',
                                 category='Notifications'))


admin.add_view(ThisYearNotification(name='This Year\'s Notifications',
                                    endpoint='notifications/this-year',
                                    category='Notifications'))

admin.add_view(ThisYearNotificationPMS(name='This Year\'s Notifications (PMS)',
                                    endpoint='notifications/this-year-pms',
                                    category='Notifications'))


admin.add_view(ThisMonthNotification(name='This Month\'s Notifications',
                                    endpoint='notifications/this-month',
                                    category='Notifications'))


admin.add_view(
    NotificationLogView(
        NotificationLog, db.session,
        name='Logs',
        endpoint='notifications/logs',
        category='Notifications'
    )
)


admin.add_view(FirstPMView(EmailNotification, db.session,
                           name='First PM',
                           endpoint='notifications/first-pm',
                           category='Notifications'))

admin.add_view(SecondPMView(EmailNotification, db.session,
                            name='Second PM',
                            endpoint='notifications/second-pm',
                            category='Notifications'))

admin.add_view(
    BatteryReplacementView(
        EmailNotification, db.session,
        name='Battery Replacement',
        endpoint='notifications/battery-replacement',
        category='Notifications',
    )
)

admin.add_view(
    LicenseRenewalView(
        EmailNotification, db.session,
        name='License Renewal',
        endpoint='notifications/license-renewal',
        category='Notifications'
    )
)

admin.add_view(
    LTORegistrationView(
        EmailNotification, db.session,
        name='LTO Registration',
        endpoint='notifications/lto-registration',
        category='Notifications'
    )
)

admin.add_view(
    PrimaryLeaseExpirationView(
        EmailNotification, db.session,
        name='Primary Lease Expiration',
        endpoint='notifications/primary-lease-expiration',
        category='Notifications'))


admin.add_view(
    SecondaryLeaseExpirationView(
        EmailNotification, db.session,
        name='Secondary Lease Expiration',
        endpoint='notifications/secondary-lease-expiration',
        category='Notifications'))
