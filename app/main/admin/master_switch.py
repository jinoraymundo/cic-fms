from flask_admin.contrib.sqla import ModelView
from ... import admin, db
from ...models import NotificationSwitch
from flask import request


class NotificationSwitchView(ModelView):
    can_create = False
    can_delete = False
    can_export = True

    def update_model(self, form, model):
        for field in form:
            if (field.type == "BooleanField") and (field.name not in request.form):
                field.data = False
        return super(NotificationSwitchView, self).update_model(form, model)


admin.add_view(NotificationSwitchView(NotificationSwitch, db.session,
                                      name='Notification Switch',
                                      endpoint='notification-switch'))
