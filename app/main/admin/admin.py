import calendar
import collections
import operator
from datetime import date
from dateutil.relativedelta import relativedelta
from flask import url_for, redirect, request

from flask_login import current_user

from flask_admin import Admin, BaseView, expose

from ... import admin, db
from ...models import Vehicle, Incident, AccidentNature


ytd = date.today().year
months = [x for x in calendar.month_name]
months_dict = collections.OrderedDict({v: [] for k, v in enumerate(months)})

class MyHomeView(BaseView):
    @expose('/')
    def index(self):
        incidents = Incident.query.all()
        vehicles = Vehicle.query.all()
        years = [x for x in xrange(ytd, 2014, -1)]
        acquisition_years = [x for x in xrange(ytd, 2009, -1)]
        vehicle_incidents = {}
        acquired_disposed = {}
        for year in years:
            vehicle_incidents[year] = {
                'January': 0,
                'February': 0,
                'March': 0,
                'April': 0,
                'May': 0,
                'June': 0,
                'July': 0,
                'August': 0,
                'September': 0,
                'October': 0,
                'November': 0,
                'December': 0,
            }

        for year in acquisition_years:
            acquired_disposed[year] = {
                'acquired': 0,
                'disposed': 0,
            }
        by_nature = {}
        per_sbu = {}

        acquired_disposed['unknown'] = {
            'acquired': 0,
            'disposed': 0,
        }

        for vehicle in vehicles:
            lease = vehicle.get_primary_lease()
            #if not lease:
            #    vehicle.get_secondary_lease()

            if vehicle.status == 'DISPOSED/SOLD':
                try:
                    year_disposed = vehicle.date_disposed.year
                    acquired_disposed[year_disposed]['disposed'] += 1
                except:
                    acquired_disposed['unknown']['disposed'] += 1
            else:
                try:
                    year_acquired = lease[0].contract_start.year
                    acquired_disposed[year_acquired]['acquired'] += 1
                except:
                    pass

        for incident in incidents:
            year = incident.date.year
            month = incident.date.month

            # by year
            try:
                vehicle_incidents[year][incident.date.strftime("%B")] += 1
            except KeyError:
                pass

            # by nature
            nature = incident.accident_nature
            if year == ytd:
                try:
                    by_nature[nature] += 1
                except KeyError:
                    by_nature[nature] = 1

            # by BU
            # BU = incident.assignment.employee.BU
            # if year == ytd:
            #     try:
            #         per_bu[BU] += 1
            #     except KeyError:
            #         per_bu[BU] = 1

            # by SBU
            SBU = incident.assignment.employee.SBU
            if year == ytd:
                try:
                    per_sbu[SBU] += 1
                except KeyError:
                    per_sbu[SBU] = 1


        return self.render('admin/index.html',
                           years=years, months_sorted=months,
                           vehicle_incidents=vehicle_incidents,
                           by_nature=by_nature, per_sbu=per_sbu,
                           acquired_disposed=sorted(acquired_disposed.items(), key=operator.itemgetter(0)),
                           acquisition_years=acquisition_years,
                           ytd=ytd)

    def is_accessible(self):
        return current_user.is_authenticated


    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


# admin = Admin(index_view=MyHomeView())
admin.add_view(MyHomeView(name='Dashboard', url='/', endpoint='/dashboard'))
