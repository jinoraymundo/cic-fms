import calendar
import collections
import operator

from flask import render_template, redirect, url_for, request, render_template
from flask_weasyprint import HTML, render_pdf
from flask_login import login_required
from . import main
from ..models import Incident, Vehicle, EmailTemplate
from datetime import date


ytd = date.today().year
months = [x for x in calendar.month_name]
months_dict = collections.OrderedDict({v: [] for k, v in enumerate(months)})


@main.route('/')
@login_required
def index():
    return redirect(url_for('admin.index'))


@main.route('/print-incident')
@login_required
def print_incident():
    incident = Incident.query.filter_by(id=request.args['incident']).one()
    html = render_template('admin/incident/print.html',
                           model=incident)
    return render_pdf(HTML(string=html))


@main.route('/print-vehicle-details')
@login_required
def print_vehicle_details():
    vehicle = Vehicle.query.filter_by(id=request.args['vehicle']).one()
    html = render_template('admin/vehicle/print.html', model=vehicle)
    return render_pdf(HTML(string=html))


@main.route('/test-email')
def test_email():
    mail_template = EmailTemplate.query.filter(
        EmailTemplate.name == 'First PM').one_or_none()
    return render_template('mail/mail.html', recipients=[],
        poster='cid:/static/notification_images/' + mail_template.poster)


@main.route('/print-dashboard')
@login_required
def print_dashboard():
    incidents = Incident.query.all()
    vehicles = Vehicle.query.all()
    years = [x for x in xrange(ytd, 2014, -1)]
    acquisition_years = [x for x in xrange(ytd, 2009, -1)]
    vehicle_incidents = {}
    acquired_disposed = {}
    for year in years:
        vehicle_incidents[year] = {
            'January': 0,
            'February': 0,
            'March': 0,
            'April': 0,
            'May': 0,
            'June': 0,
            'July': 0,
            'August': 0,
            'September': 0,
            'October': 0,
            'November': 0,
            'December': 0,
        }

    for year in acquisition_years:
        acquired_disposed[year] = {
            'acquired': 0,
            'disposed': 0,
        }
    by_nature = {}
    per_sbu = {}

    acquired_disposed['unknown'] = {
        'acquired': 0,
        'disposed': 0,
    }

    for vehicle in vehicles:
        lease = vehicle.get_primary_lease()
        if not lease:
            vehicle.get_secondary_lease()

        if vehicle.status == 'DISPOSED/SOLD':
            try:
                year_disposed = vehicle.date_disposed.year
                acquired_disposed[year_disposed]['disposed'] += 1
            except:
                acquired_disposed['unknown']['disposed'] += 1
        else:
            try:
                year_acquired = lease[0].contract_start.year
                acquired_disposed[year_acquired]['acquired'] += 1
            except:
                pass

    for incident in incidents:
        year = incident.date.year
        month = incident.date.month

        # by year
        try:
            vehicle_incidents[year][incident.date.strftime("%B")] += 1
        except KeyError:
            pass

        # by nature
        nature = incident.accident_nature
        if year == ytd:
            try:
                by_nature[nature] += 1
            except KeyError:
                by_nature[nature] = 1

        # by SBU
        SBU = incident.assignment.employee.SBU
        if year == ytd:
            try:
                per_sbu[SBU] += 1
            except KeyError:
                per_sbu[SBU] = 1

    html = render_template('print_dashboard.html', years=years,
                           vehicle_incidents=vehicle_incidents,
                           by_nature=by_nature, per_sbu=per_sbu,
                           acquired_disposed=sorted(acquired_disposed.items(), key=operator.itemgetter(0)),
                           acquisition_years=acquisition_years,
                           months_sorted=months, ytd=ytd)
    # return render_pdf(HTML(string=html))


    return render_template('print_dashboard.html',
                       years=years, months_sorted=months,
                       vehicle_incidents=vehicle_incidents,
                       by_nature=by_nature, per_sbu=per_sbu,
                       acquired_disposed=sorted(acquired_disposed.items(), key=operator.itemgetter(0)),
                       acquisition_years=acquisition_years,
                       ytd=ytd)
