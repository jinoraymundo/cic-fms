from flask_admin.contrib.sqla import ModelView
from .. import admin, db
from ..models import Employee,\
    EmployeeEntity as Entity,\
    EmployeeDepartment as Department,\
    EmployeeSection as Section,\
    EmployeeClassification as Classification,\
    EmployeeClassificationOther as ClassificationOther,\
    EmployeeSBU as SBU,\
    EmployeeCostCenter as CostCenter,\
    EmployeeBU as BU,\
    EmployeeLocation as Location,\
    EmployeePosition as Position,\
    EmployeeTOT as TOT,\
    HondaCertification as HC,\
    DesignatedDriver as DD,\
    ContactNumber,\
    LeasingInformation as LI
from ..models import Vehicle,\
    Dealer
from ..models import AccidentNature, Incident
from ..models import User, Role
from flask_admin.model.form import InlineFormAdmin


core_columns = [
    'entity', 'employee_no', 'email', 'SBU', 'BU', 'department',
    'section', 'cost_center', 'classification', 'classification_code',
    'location', 'position', 'doh', 'tot', 'gender', 'license_no',
    'date_issued', 'license_expiry', 'remarks',
]


class HCInlineModelForm(InlineFormAdmin):
    form_choices = {
        'remarks': [
            (u'passed-all', u'PASSED/CAN DRIVE SAFELY'),
            (u'passed-mt', u'PASSED/CAN DRIVE SAFELY(M/T)'),
            (u'passed-at', u'PASSED/CAN DRIVE SAFELY(A/T)'),
            (u'passed-nr', u'PASSED BUT NO LATEST RECORD'),
            (u'failed', u'FAILED/CANNOT DRIVE SAFELY'),
            (u'failed-np', u'FAILED EXAM/NO PRACTICAL'),
            (u'nr', u'NO RECORD/FOR CHECKING'),
            (u'nl', u'NO LICENSE'),
        ]
    }

    form_widget_args = dict(
        date=dict(type='date')
    )

    column_labels = dict(mt='M/T', at='A/T')


class LIIinlineModelForm(InlineFormAdmin):
    form_widget_args = dict(
        contract_start=dict(type='date'),
        contract_end=dict(type='date')
    )

    column_labels = dict(
        is_primary='Is Primary Lease?')


class UserView(ModelView):
    column_list = ['email', 'roles']
    form_columns = ['email', 'password']


class RoleView(ModelView):
    column_list = ['name', 'description', ]
    form_columns = ['name', 'description', ]


class EmployeeView(ModelView):
    can_export = True
    can_view_details = True
    details_template = 'admin/employee/details.html'
    inline_models = (ContactNumber, HCInlineModelForm(HC), DD)
    column_filters = [
        'last_name', 'first_name', 'entity', 'employee_no', 'email', 'doh',
        'SBU', 'BU', 'department', 'position', 'location',
        'license_no', 'classification_code',
    ]
    column_default_sort = 'last_name'
    column_list = [
        'name', 'entity', 'employee_no', 'email',
        'SBU', 'BU', 'department', 'section', 'position', 'location',
        'doh', 'gender', 'license_no', 'classification',
        'classification_code',
    ]
    column_sortable_list = ()
    form_columns = [
        'first_name', 'last_name', 'middle_name', ] + core_columns
    column_labels = dict(email='Email Address',
                         SBU='SBU',
                         BU='BU',
                         tot='Tool of Trade / Fringe Benefit',
                         doh='DOH')

    form_widget_args = dict(
        date_issued=dict(type='date'),
        license_expiry=dict(type='date'),
        doh=dict(type='date'))

    form_choices = {
        'gender': [
            (u'male', u'Male'),
            (u'female', u'Female'),
        ]
    }

    def get_details_columns(self):
        return [
            ('name', 'Name: '),
            ('employee_no', 'Employee #: '),
            ('email', 'Email Address: '),
            ('license_no', 'License #: '),
            ('date_issued', 'Date Issued: '),
            ('license_expiry', 'Expires on: '),
            ('vehicles', 'Assigned Vehicles:'),
            ('honda_certifications', 'Honda Certification Record: '),
            ('SBU', 'SBU: '),
            ('BU', 'BU: '),
            ('cost_center', 'Cost Center: '),
            ('classification', 'Classification: '),
            ('department', 'Department: '),
            ('location', 'Location: '),
            ('position', 'Position: '),
            ('tot', 'Tool of Trade / Fringe Benefit: ')
        ]


class VehicleView(ModelView):

    FUEL_TYPES = [
        (u'gas', u'Gas'),
        (u'dsl', u'Diesel'),
    ]

    STATUS_TYPES = [
        (u'active', u'ACTIVE'),
        (u'bidding', u'FOR BIDDING'),
        (u'disposal', u'FOR DISPOSAL'),
        (u'unassigned', u'UNASSIGNED'),
    ]

    LEASING_COMPANIES = (
        (u'bpi_rental', u'BPI Rental'),
        (u'ccac_owned', u'CCAC Owned'),
    )
    CONTRACT_TYPES = (
        (u'operating_lease', u'OPERATING LEASE'),
        (u'owned', u'OWNED'),
    )

    can_export = True
    can_view_details = True
    inline_models = (LIIinlineModelForm(LI), )
    column_filters = ['plate_no', 'cs_no', ]
    column_default_sort = 'plate_no'
    column_list = [
        'primary_assignee', 'assignees', 'plate_no', 'cs_no', 'sticker_no',
        'engine_no', 'year', 'make_model', 'fuel_type', 'color',
        'lto_mv_file_no', 'chassis_no', 'status_string', 'date_assigned',
        'oac_policy_no', 'effectivity_period_start', 'effectivity_period_end',
        'leasing_company_string', 'contract_type_string',
        'acquisition_cost', 'dealer', 'shell_fleet_card', 'credit_limit',
        'with_rfid', 'remarks',
    ]
    column_sortable_list = ('plate_no', )
    form_columns = [
        'primary_assignee', 'assignees', 'plate_no', 'cs_no', 'sticker_no',
        'engine_no', 'year', 'make_model', 'fuel', 'color', 'lto_mv_file_no',
        'chassis_no', 'status', 'date_assigned', 'oac_policy_no',
        'effectivity_period_start', 'effectivity_period_end',
        'leasing_company', 'contract_type',
        'acquisition_cost', 'dealer', 'shell_fleet_card',
        'credit_limit', 'with_rfid', 'remarks',
    ]
    column_labels = dict(
        cs_no='CS #',
        plate_no='Plate #',
        sticker_no='Sticker #',
        engine_no='Engine #',
        make_model='Make/Model',
        lto_mv_file_no='LTO MV File #',
        oac_policy_no='OAC Policy #',
        fuel_type='Fuel Type',
        status_string='Status',
        leasing_company_string='Leasing Company',
        contract_type_string='Contract Type'
    )

    form_choices = {
        'fuel': FUEL_TYPES,
        'status': STATUS_TYPES,
        'leasing_company': LEASING_COMPANIES,
        'contract_type': CONTRACT_TYPES,
    }

    form_widget_args = dict(
        date_assigned=dict(type='date'),
        effectivity_period_start=dict(type='date'),
        effectivity_period_end=dict(type='date'),
    )


class HondaCertificationView(ModelView):
    can_export = True
    form_choices = {
        'remarks': [
            (u'passed-all', u'PASSED/CAN DRIVE SAFELY'),
            (u'passed-mt', u'PASSED/CAN DRIVE SAFELY(M/T)'),
            (u'passed-at', u'PASSED/CAN DRIVE SAFELY(A/T)'),
            (u'passed-nr', u'PASSED BUT NO LATEST RECORD'),
            (u'failed', u'FAILED/CANNOT DRIVE SAFELY'),
            (u'failed-np', u'FAILED EXAM/NO PRACTICAL'),
            (u'nr', u'NO RECORD/FOR CHECKING'),
            (u'nl', u'NO LICENSE'),
        ]
    }

    column_filters = ['employee', ]

    form_widget_args = dict(
        date=dict(type='date')
    )

    column_labels = dict(mt='M/T', at='A/T')


admin.add_view(UserView(User, db.session, name='Users',
                        endpoint='users', category='Admin'))
admin.add_view(RoleView(Role, db.session, name='Roles',
                        endpoint='roles', category='Admin'))

admin.add_view(EmployeeView(Employee, db.session, name='Employees',
                            endpoint='employee_master', category='Employee'))

admin.add_view(ModelView(Entity, db.session,
                         name='Entities',
                         endpoint='entities',
                         category='Lookups'))

admin.add_view(ModelView(ClassificationOther, db.session,
                         name='Classifications',
                         endpoint='classifications',
                         category='Lookups'))

admin.add_view(ModelView(Classification, db.session,
                         name='Classification Codes',
                         endpoint='classification_codes',
                         category='Lookups'))
admin.add_view(ModelView(Department, db.session, name='Departments',
                         endpoint='departments', category='Lookups'))

admin.add_view(ModelView(Section, db.session,
                         name='Sections',
                         endpoint='sections',
                         category='Lookups'))

admin.add_view(ModelView(SBU, db.session, name='SBU',
                         endpoint='SBU', category='Lookups'))
admin.add_view(ModelView(BU, db.session, name='BU', endpoint='BU',
                         category='Lookups'))
admin.add_view(ModelView(Location, db.session, name='Location',
                         endpoint='Location', category='Lookups'))
admin.add_view(ModelView(Position, db.session, name='Position',
                         endpoint='Position', category='Lookups'))
admin.add_view(ModelView(TOT, db.session,
                         name='Fringe Benefit / Tool of Trade Classification',
                         endpoint='FB/TOT', category='Lookups'))
admin.add_view(ModelView(CostCenter, db.session, name='Cost Centers',
                         endpoint='cost_centers', category='Lookups'))
admin.add_view(HondaCertificationView(HC, db.session,
                                      name='Honda Certifications',
                                      endpoint='honda_certifications',
                                      category='Employee'))

admin.add_view(VehicleView(Vehicle, db.session, name='Vehicles',
                           endpoint='vehicle_master', category='Vehicle'))
admin.add_view(ModelView(Dealer, db.session, name='Dealers',
                         endpoint='dealers', category='Vehicle'))

# FLEET INCIDENT
class AccidentNatureView(ModelView):
    form_columns = ['name', ]


class FleetIncidentView(ModelView):
    can_export = True
    form_columns = [
        'fleet_incident_id',
        'vehicle',
        'accident_nature',
        'date',
        'location',
        'details',
        'damaged_portion',
        'remarks',
    ]
    form_widget_args = dict(
        date=dict(type='date')
    )
    column_filters = [
        'vehicle',
        'accident_nature',
        'date',
        'location',
    ]


admin.add_view(FleetIncidentView(Incident, db.session, name='Incidents',
                         endpoint='incidences',
                         category='Fleet Incidents Monitoring'))
admin.add_view(AccidentNatureView(AccidentNature, db.session,
                         name='Nature of Accidents',
                         endpoint='nature_accidents',
                         category='Fleet Incidents Monitoring'))
