from datetime import date
from dateutil.relativedelta import relativedelta
from .models import Vehicle, EmailTemplate, NotificationSwitch
from .models import NotificationLog
from app import mail

from threading import Thread

from flask_mail import Message
from flask import render_template, current_app

import os
import time
import os.path as op

import app
from app import db

import calendar
import collections


ytd = date.today().year
mtd = date.today().month
months = [x for x in calendar.month_name]
months_dict = collections.OrderedDict({v: [] for k, v in enumerate(months)})


file_path = op.join(
    op.dirname(
        op.dirname(op.dirname(__file__))), 'static', 'notification_images')

poster_path = op.join(
    op.dirname(op.dirname(__file__)),
    'app',
    'static',
    'notification_images',
)


attachment_path = op.join(
    op.dirname(op.dirname(op.dirname(__file__))),
    'cic-fms',
    'app',
    'static',
    'notification_attachments',
)


# attachment_path = op.join(
#     op.dirname(
#         op.dirname(op.dirname(__file__))), 'static', 'notification_attachments')

today = date.today()
# today = date(2018, 1, 30)
# today = date(2018, 5, 2)

def get_first_pm(vehicles):
    first_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.pms:
                # if vehicle.pms['first_pm'] == today:
                if vehicle.notifications['first_pm_due'] == today:
                    first_pms.append(vehicle)
                    continue
                if vehicle.pms['first_pm'] == today:
                    first_pms.append(vehicle)
                    continue
        except (KeyError, TypeError):
            pass
    return sorted(first_pms, key=lambda vehicle: vehicle.next_pm)    


def get_succeeding_pm_first(vehicles):
    second_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.pms:
                if vehicle.pms['second_pm'] == today:
                    second_pms.append(vehicle)
                    continue
        except (KeyError, TypeError):
            pass
    return sorted(second_pms, key=lambda vehicle: vehicle.next_pm)


# escalate
def get_succeeding_pm_third(vehicles):
    escalate_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.pms:
                if vehicle.pms['third_pm'] == today:
                    escalate_pms.append(vehicle)
                    continue
        except (KeyError, TypeError):
            pass
    return sorted(escalate_pms, key=lambda vehicle: vehicle.next_pm)


def get_succeeding_pm_second(vehicles):
    second_pms = []
    for vehicle in vehicles:
        try:
            if vehicle.pms:
                if vehicle.next_pm == today:
                    second_pms.append(vehicle)
        except (KeyError, TypeError):
            pass
    return sorted(second_pms, key=lambda vehicle: vehicle.next_pm)


# def get_succeeding_pm_third(vehicles):
#     second_pms = []
#     for vehicle in vehicles:
#         try:
#             if vehicle.pms:
#                 if vehicle.next_pm == today:
#                     second_pms.append(vehicle)
#         except (KeyError, TypeError):
#             pass
#     return sorted(second_pms, key=lambda vehicle: vehicle.next_pm)


def get_lto_reg(vehicles):
    lto = []
    for vehicle in vehicles:
        try:
            to_compare = today + relativedelta(months=1)
            if vehicle.notifications['lto_reg'] == to_compare:
                lto.append(vehicle)
            if vehicle.notifications['lto_reg'].year < ytd and \
                    vehicle.notifications['lto_reg'].month == mtd and \
                    vehicle.notifications['lto_reg'].day == today.day:
                lto.append(vehicle)
        except:
            pass
    return sorted(lto, key=lambda vehicle: vehicle.notifications['lto_reg'])


def get_primary_lease_expiration(vehicles):
    ple = []
    for vehicle in vehicles:
        if vehicle.primary_lease_contract_end:
            lease_notification = vehicle.notifications['primary_lease_expiration']
            if lease_notification == today:
                ple.append(vehicle)
    return sorted(ple, key=lambda vehicle: vehicle.notifications['primary_lease_expiration'])


def get_secondary_lease_expiration(vehicles):
    sle = []
    for vehicle in vehicles:
        if vehicle.secondary_lease_contract_end:
            lease_notification = vehicle.notifications['secondary_lease_expiration']
            if lease_notification == today:
                sle.append(vehicle)
    return sorted(sle, key=lambda vehicle: vehicle.notifications['secondary_lease_expiration'])


def get_battery_replacement(vehicles):
    battery = []
    for vehicle in vehicles:
        try:
            if vehicle.notifications['battery'] == today:
                battery.append(vehicle)
        except:
            pass
    return sorted(battery, key=lambda vehicle: vehicle.notifications['battery'])


def save_to_log(recipient, notification_type):
    log = NotificationLog(
        recipient=recipient,
        notification_type=notification_type
    )
    try:
        db.session.add(log)
        db.session.commit()
    except Exception, ex:
        db.session.rollback()


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_mail(recipients, mail_template, send_to_fleet_team=False):
    notification_switch = NotificationSwitch.query.first()

    if send_to_fleet_team:
        print "SENDING EMAIL TO FLEET TEAM ONLY: THIRD REMINDER"
        with mail.connect() as conn:
            for vehicle in recipients:
                msg = Message(recipients=['randy.tayzon@cbsi.ph', 'lorenzolowey.moreno@cbsi.ph', ],
                    body=vehicle.primary_assignee.email, subject=mail_template.name, sender='facilitiesandfleetmanagement@ccac.com.ph')

                msg.subject += "%s" % vehicle.cs_no
                if vehicle.plate_no != 'NO LTO PLATE':
                    msg.subject += " %s" % vehicle.plate_no
                msg.subject += " - "
                msg.subject += "%s %s %s" % (vehicle.year, vehicle.make_model, vehicle.color)

                msg.html = render_template('mail/mail.html', poster='cid:poster',
                    message_body="FOR ESCALATION")
                with open(op.join(poster_path, mail_template.poster)) as f:
                    msg.attach(
                        mail_template.poster,
                        "image/png",
                        f.read(),
                        headers=[
                            ['Content-ID', '<poster>'],
                        ]
                    )
                if mail_template.email_template_attachments:
                    for attachment in mail_template.email_template_attachments:
                        if attachment.attachment:
                            with open(op.join(attachment_path, attachment.attachment)) as f:
                                msg.attach(
                                    attachment.attachment,
                                    "application/pdf",
                                    f.read(),
                                )
                conn.send(msg)
                print "sending to: %s" % vehicle.primary_assignee.email
                save_to_log(vehicle.primary_assignee.email, mail_template.name)

    else:
        # send to intended recipients
        if notification_switch.on_off:
            print "SENDING EMAIL TO INTENDED RECIPIENTS: NOTIFICATION SWITCH IS ON"
            with mail.connect() as conn:
                for vehicle in recipients:
                    msg = Message(recipients=[vehicle.primary_assignee.email, ],
                        body=vehicle.primary_assignee.email, subject=mail_template.name, sender='facilitiesandfleetmanagement@ccac.com.ph')
                    msg.cc = ['randy.tayzon@cbsi.ph', 'lorenzolowey.moreno@cbsi.ph', ]

                    msg.subject += " %s" % vehicle.cs_no
                    if vehicle.plate_no != 'NO LTO PLATE':
                        msg.subject += " %s" % vehicle.plate_no
                    msg.subject += " - "
                    msg.subject += "%s %s %s" % (vehicle.year, vehicle.make_model, vehicle.color)

                    msg.html = render_template('mail/mail.html', poster='cid:poster',
                        message_body=mail_template.template)
                    with open(op.join(poster_path, mail_template.poster)) as f:
                        msg.attach(
                            mail_template.poster,
                            "image/png",
                            f.read(),
                            headers=[
                                ['Content-ID', '<poster>'],
                            ]
                        )
                    if mail_template.email_template_attachments:
                        for attachment in mail_template.email_template_attachments:
                            if attachment.attachment:
			        path = op.join(attachment_path, attachment.attachment)		
                                with open(path) as f:
                                    msg.attach(
                                        attachment.attachment,
                                        "application/pdf",
                                        f.read(),
                                     )
                    conn.send(msg)
                    print "sending to: %s" % vehicle.primary_assignee.email
                    save_to_log(vehicle.primary_assignee.email, mail_template.name)
        else:  # send to fleet team only
            msg.recipients = ['facilitiesandfleetmanagement@ccac.com.ph', ]
            print "SENDING EMAIL TO FLEET TEAM ONLY: NOTIFICATION SWITCH IS OFF"
            mail.send(msg)
            for item in msg.recipients:
                save_to_log(item, mail_template.name)


def scan_all(app, db):
    vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
    first_pm_notification_recipients = get_first_pm(vehicles)
    succeeding_pm_notification_recipients_first = get_succeeding_pm_first(vehicles)
    escalate_pm_notification_recipients = get_succeeding_pm_third(vehicles)
    battery_notification_recipients = get_battery_replacement(vehicles)
    lto_notification_recipients = get_lto_reg(vehicles)
    # scan_drivers_license(today)
    # lto_notification_recipients = scan_lto_registration(today)

    if first_pm_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'First Periodic Maintenance Reminder').one_or_none()
        if mail_template:
            send_mail(first_pm_notification_recipients, mail_template)

    if succeeding_pm_notification_recipients_first:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'Periodic Maintenance Reminder').one_or_none()
        if mail_template:
            send_mail(succeeding_pm_notification_recipients_first, mail_template)

    if escalate_pm_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'Periodic Maintenance Reminder').one_or_none()
        if mail_template:
            send_mail(escalate_pm_notification_recipients, mail_template, send_to_fleet_team=True)


    # if succeeding_pm_notification_recipients_second:
    #     mail_template = EmailTemplate.query.filter(
    #         EmailTemplate.name == 'Periodic Maintenance Reminder').one_or_none()
    #     if mail_template:
    #         send_mail(succeeding_pm_notification_recipients_second, mail_template)

    # if succeeding_pm_notification_recipients_third:
    #     mail_template = EmailTemplate.query.filter(
    #         EmailTemplate.name == 'Periodic Maintenance Reminder').one_or_none()
    #     if mail_template:
    #         send_mail(succeeding_pm_notification_recipients_third, mail_template, True)

    if battery_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'Battery Replacement').one_or_none()
        if mail_template:
            send_mail(battery_notification_recipients, mail_template)

    if lto_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'LTO Registration').one_or_none()
        if mail_template:
            send_mail(lto_notification_recipients, mail_template)
