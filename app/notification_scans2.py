from datetime import date
from dateutil.relativedelta import relativedelta
from .models import Vehicle, Employee, EmailTemplate

from sqlalchemy import and_


def scan_first_pm(today):
    to_notify = []
    notification_date = today + relativedelta(months=1)
    vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
    for vehicle in vehicles:
        try:
            pm_schedule = vehicle.pm_schedule
            if pm_schedule['first_pm'] and \
                    pm_schedule['first_pm'] == notification_date:
                if vehicle.primary_assignee:
                    to_notify.append(vehicle.primary_assignee)
        except TypeError:
            pass
    return to_notify


def scan_second_pm(today):
    to_notify = []
    notification_date = today + relativedelta(months=1)
    vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
    for vehicle in vehicles:
        try:
            pm_schedule = vehicle.pm_schedule
            if pm_schedule['second_pm'] and \
                    pm_schedule['second_pm'] == notification_date:
                if vehicle.primary_assignee:
                    to_notify.append(vehicle.primary_assignee)
        except TypeError:
            pass
    return to_notify


def scan_battery_replacement(today):
    to_notify = []
    vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
    for vehicle in vehicles:
        try:
            pm_schedule = vehicle.pm_schedule
            battery_replacement_schedule = \
                pm_schedule['first_pm'] + relativedelta(months=24)
            if pm_schedule['first_pm'] and \
                    battery_replacement_schedule == today:
                if vehicle.primary_assignee:
                    to_notify.append(vehicle.primary_assignee)
        except TypeError:
            pass
    return to_notify


# def scan_drivers_license(today):
#     notification_date = today + relativedelta(days=7)
#     employees = Employee.query.filter(
#         and_(Employee.license_no.isnot(None),
#              Employee.license_expiry.isnot(None)))
#     for employee in employees:
#         print employee


def scan_lto_registration(today):
    to_notify = []
    notification_date = today + relativedelta(months=1)
    vehicles = Vehicle.query.filter(Vehicle.status == 'ACTIVE')
    for vehicle in vehicles:
        try:
            pm_schedule = vehicle.pm_schedule

            lto_registration_schedule = \
                pm_schedule['first_pm'] + relativedelta(months=36) - \
                relativedelta(months=pm_schedule['details'].interval)

            # first 3 years
            if pm_schedule['first_pm'] and \
                    lto_registration_schedule == notification_date:
                if vehicle.primary_assignee:
                    to_notify.append(vehicle.primary_assignee)

            # annual - must be first day of the month
            # if today.day == 1:
            if True:
                try:
                    # should be same month as with last number in plate
                    if today.month == int(vehicle.plate_no[-1]) and \
                            pm_schedule['first_pm'].month == today.month:
                        date_diff = relativedelta(
                            today, pm_schedule['first_pm'])
                        if date_diff and date_diff.years >= 3:
                            to_notify.append(vehicle.primary_assignee)
                except ValueError:
                    pass
        except TypeError:
            pass
    return to_notify


def send_mail(recipients, mail_template):
    print recipients, mail_template


def scan_all(app, db):
    today = date.today()
    first_pm_notification_recipients = scan_first_pm(today)
    second_pm_notification_recipients = scan_second_pm(today)
    battery_notification_recipients = scan_battery_replacement(today)
    # scan_drivers_license(today)
    lto_notification_recipients = scan_lto_registration(today)

    if first_pm_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'First PM').one_or_none()
        if mail_template:
            send_mail(first_pm_notification_recipients, mail_template)

    if second_pm_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'Second PM').one_or_none()
        if mail_template:
            send_mail(second_pm_notification_recipients, mail_template)

    if battery_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'Battery Replacement').one_or_none()
        if mail_template:
            send_mail(battery_notification_recipients, mail_template)

    if lto_notification_recipients:
        mail_template = EmailTemplate.query.filter(
            EmailTemplate.name == 'LTO Registration').one_or_none()
        if mail_template:
            send_mail(lto_notification_recipients, mail_template)
