from . import db, login_manager
from flask_security import UserMixin, RoleMixin
from werkzeug.security import generate_password_hash
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.ext.hybrid import hybrid_property

from datetime import date

from dateutil.relativedelta import relativedelta


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class GenericLookupTable(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)

    def __repr__(self):
        return self.name


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    users = db.relationship(
        'User',
        secondary='user_roles',
        backref='roles'
    )

    def __repr__(self):
        return self.name


class UserRoles(db.Model):
    __tablename__ = 'user_roles'

    user_id = db.Column(
        db.Integer,
        db.ForeignKey('user.id', ondelete='CASCADE'),
        primary_key=True
    )
    user = db.relationship('User')

    role_id = db.Column(
        db.Integer,
        db.ForeignKey('role.id', ondelete='CASCADE'),
    )
    role = db.relationship('Role')


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String(128))
    email = db.Column(db.String(255), unique=True)
    active = db.Column(db.Boolean())

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def verify_password(self, password):
        return self.password == password


# Employee Related Models
class EmployeeEntity(GenericLookupTable):
    __tablename__ = 'employee_entities'


class EmployeeSBU(GenericLookupTable):
    __tablename__ = 'employee_SBUs'


class EmployeeLocation(GenericLookupTable):
    __tablename__ = 'employee_locations'


class EmployeeBU(GenericLookupTable):
    __tablename__ = 'employee_BUs'


class EmployeePosition(GenericLookupTable):
    __tablename__ = 'employee_positions'


class EmployeeTOT(GenericLookupTable):
    __tablename__ = 'employee_TOTs'


class EmployeeCostCenter(GenericLookupTable):
    __tablename__ = 'employee_cost_centers'


class EmployeeClassification(GenericLookupTable):
    __tablename__ = 'employee_classifications'


class EmployeeClassificationOther(GenericLookupTable):
    __tablename__ = 'employee_classifications_other'


class EmployeeSection(GenericLookupTable):
    __tablename__ = 'employee_sections'


class EmployeeDepartment(GenericLookupTable):
    __tablename__ = 'employee_departments'


class VehiclesToEmployees(db.Model):
    __tablename__ = 'vehicles_users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True,
                   unique=True)

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='CASCADE'),
        primary_key=False
    )
    employee = db.relationship('Employee')

    vehicle_id = db.Column(
        db.Integer,
        db.ForeignKey('vehicles.id', ondelete='CASCADE'),
        primary_key=False
    )
    vehicle = db.relationship('Vehicle')

    assigned_start = db.Column(db.Date, nullable=True)
    assigned_end = db.Column(db.Date)
    is_primary = db.Column(db.Boolean, default=False)
    tot_fringe = db.Column(db.String)

    def __unicode__(self):
        return "%s - %s" % (self.vehicle, self.employee)

    __table_args__ = (
        db.UniqueConstraint('employee_id', 'vehicle_id',
                            'assigned_start', 'assigned_end',
                            name='uix_ve_1'),
        db.CheckConstraint('assigned_start < assigned_end',
                           name='cc_ve_1'),
    )


class Employee(db.Model):
    __tablename__ = 'employees'
    id = db.Column(db.Integer, primary_key=True)

    employee_no = db.Column(db.String, unique=True)
    first_name = db.Column(db.String(128), nullable=False)
    middle_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=True)
    doh = db.Column(db.Date)
    gender = db.Column(db.String(12))
    license_no = db.Column(db.String(11), unique=True)
    date_issued = db.Column(db.Date)
    license_expiry = db.Column(db.Date)
    license_type = db.Column(db.String)
    waiver = db.Column(db.Boolean, default=False)
    is_sales = db.Column(db.Boolean, default=False)

    vehicles = db.relationship(
        'Vehicle',
        secondary=VehiclesToEmployees.__tablename__,
        backref='assignees'
    )

    entity_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_entities.id', ondelete='SET NULL'),
        nullable=True
    )
    entity = db.relationship('EmployeeEntity')

    SBU_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_SBUs.id', ondelete='SET NULL'),
        nullable=True
    )
    SBU = db.relationship('EmployeeSBU')

    cost_center_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_cost_centers.id', ondelete='SET NULL'),
        nullable=True
    )
    cost_center = db.relationship('EmployeeCostCenter')

    classification_code_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_classifications.id', ondelete='SET NULL'),
        nullable=True
    )
    classification_code = db.relationship('EmployeeClassification')

    classification_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_classifications_other.id',
                      ondelete='SET NULL'),
        nullable=True
    )
    classification = db.relationship('EmployeeClassificationOther')

    department_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_departments.id', ondelete='SET NULL'),
        nullable=True
    )
    department = db.relationship('EmployeeDepartment')

    section_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_sections.id', ondelete='SET NULL'),
        nullable=True)
    section = db.relationship('EmployeeSection')

    BU_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_BUs.id', ondelete='SET NULL'),
        nullable=True
    )
    BU = db.relationship('EmployeeBU')

    location_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_locations.id', ondelete='SET NULL'),
        nullable=True
    )
    location = db.relationship('EmployeeLocation')

    position_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_positions.id', ondelete='SET NULL'),
        nullable=True
    )
    position = db.relationship('EmployeePosition')

    tot_id = db.Column(
        db.Integer,
        db.ForeignKey('employee_TOTs.id', ondelete='SET NULL'),
        nullable=True
    )
    tot = db.relationship('EmployeeTOT')

    remarks = db.Column(db.Text)

    @property
    def sales_display(self):
        if self.is_sales:
            return "SALES"
        return "NON-SALES"

    @property
    def honda_certification(self):
        if self.honda_certifications:
            hcs = list(self.honda_certifications)
            recent = max(hcs, key=lambda x: x.date)
            if recent.remarks:
                if 'passed' in str(recent.remarks).lower():
                    return 'COMPLIANT'
                elif 'scheduled' in str(recent.remarks).lower():
                    return 'COMPLIANT'
                elif 'failed' in str(recent.remarks).lower():
                    return 'NON-COMPLIANT'
                else:
                    return 'NON-COMPLIANT'
            else:
                return 'UNDEFINED'
        elif self.designated_drivers:
            return "COMPLIANT"
        elif self.license_type == 'pro':
            return "COMPLIANT"
        return "NO DEFINED HONDA CERTIFICATION(S)"

    @hybrid_property
    def name(self):
        if self.middle_name:
            return u'%s, %s %s' % (self.last_name,
                                   self.first_name, self.middle_name)
        else:
            return u'%s, %s' % (self.last_name, self.first_name)

    @name.expression
    def name(cls):
        return u'%s, %s %s' % (cls.last_name,
                               cls.first_name, cls.middle_name)

    def __repr__(self):
        return self.name
        # return self.name.encode('utf-8')

    @property
    def assignment_eligible(self):
        is_eligible = False
        if self.honda_certification == "COMPLIANT":
            is_eligible = True
        if self.license_type == 'PRO':
            is_eligible = True
        if self.waiver:
            is_eligible = True
        return is_eligible

    __table_args__ = (
        db.UniqueConstraint('first_name', 'last_name'),
    )

    __mapper_args__ = {
        "order_by": last_name
    }


class HondaCertification(db.Model):
    __tablename__ = 'honda_certification'
    id = db.Column(db.Integer, primary_key=True)

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='CASCADE'),
    )
    employee = db.relationship('Employee', backref='honda_certifications',
                               single_parent=True)
    date = db.Column(db.Date)
    score = db.Column(db.Integer)
    score_items = db.Column(db.Integer, default=50)
    mt = db.Column(db.Float(precision=3, asdecimal=True,
                            decimal_return_scale=2))
    at = db.Column(db.Float(precision=3, asdecimal=True,
                            decimal_return_scale=2))
    remarks = db.Column(db.String(64))

    @hybrid_property
    def total_score(self):
        return '%s / %s' % (self.score, self.score_items)

    def __repr__(self):
        return '%s - %s' % (self.remarks, self.date)

    __table_args__ = (
        db.UniqueConstraint('employee_id', 'date', 'remarks'),
    )


class DesignatedDriver(db.Model):
    __tablename__ = 'designated_drivers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    relation = db.Column(db.String, nullable=False)
    license_no = db.Column(db.String(11), nullable=False)
    notes = db.Column(db.Text)

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='CASCADE'),
        nullable=False
    )
    employee = db.relationship('Employee', backref='designated_drivers')

    @property
    def honda_certification(self):
        if self.driver_honda_certifications:
            hcs = list(self.driver_honda_certifications)
            recent = max(hcs, key=lambda x: x.date)
            if recent.remarks:
                if 'passed' in str(recent.remarks).lower():
                    return 'COMPLIANT'
                elif 'scheduled' in str(recent.remarks).lower():
                    return 'COMPLIANT'
                elif 'failed' in str(recent.remarks).lower():
                    return 'NON-COMPLIANT'
                else:
                    return 'NON-COMPLIANT'
            else:
                return 'UNDEFINED'
        return "NO DEFINED HONDA CERTIFICATION(S)"

    def __repr__(self):
        return self.name


class DriverHondaCertification(db.Model):
    __tablename__ = 'driver_honda_certification'
    id = db.Column(db.Integer, primary_key=True)

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('designated_drivers.id', ondelete='CASCADE'),
    )
    employee = db.relationship('DesignatedDriver', backref='driver_honda_certifications',
                               single_parent=True)
    date = db.Column(db.Date)
    score = db.Column(db.Integer)
    score_items = db.Column(db.Integer, default=50)
    mt = db.Column(db.Float(precision=3, asdecimal=True,
                            decimal_return_scale=2))
    at = db.Column(db.Float(precision=3, asdecimal=True,
                            decimal_return_scale=2))
    remarks = db.Column(db.String(64))

    @hybrid_property
    def total_score(self):
        return '%s / %s' % (self.score, self.score_items)

    def __repr__(self):
        return '%s - %s' % (self.remarks, self.date)

    __table_args__ = (
        db.UniqueConstraint('employee_id', 'date', 'remarks'),
    )


class ContactNumber(db.Model):
    __tablename__ = 'contact_numbers'
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(128), nullable=False)

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='CASCADE'),
        nullable=False
    )
    employee = db.relationship('Employee', backref='contact_numbers')


# Vehicle Module
class Vehicle(db.Model):
    __tablename__ = 'vehicles'
    id = db.Column(db.Integer, primary_key=True)

    cs_no = db.Column(db.String(6), nullable=False)
    plate_no = db.Column(db.String, nullable=False)
    sticker_no = db.Column(db.String(3))
    year = db.Column(db.String(4))
    make_model = db.Column(db.String(128), nullable=False)
    fuel = db.Column(db.String)
    engine_no = db.Column(db.String, unique=True, nullable=False)
    chassis_no = db.Column(db.String, nullable=False)
    color = db.Column(db.String(50))
    lto_mv_file_no = db.Column(db.String, nullable=False)
    status = db.Column(db.String(16))

    leasing_company = db.Column(db.String(16))
    contract_type = db.Column(db.String(16))
    acquisition_cost = db.Column(db.Numeric(10, 2))

    shell_fleet_card = db.Column(db.String(25))
    credit_limit = db.Column(db.Numeric(10, 2))
    with_rfid = db.Column(db.Boolean, default=False)
    date_disposed = db.Column(db.Date)

    dealer_id = db.Column(
        db.Integer,
        db.ForeignKey('dealers.id', ondelete='CASCADE'),
    )
    dealer = db.relationship('Dealer')

    remarks = db.Column(db.Text)

    assignments = db.relationship(
        'VehiclesToEmployees',
    )

    def get_primary_lease(self):
        return [x for x in self.leasing_information
                    if x.is_primary]

    def get_secondary_lease(self):
        return [x for x in self.leasing_information
                    if not x.is_primary]

    @property
    def primary_lease_account_number(self):
        lease = self.get_primary_lease()
        if lease:
            return lease[0].lease_account_no
        return None

    @property
    def primary_lease_contract_start(self):
        lease = self.get_primary_lease()
        if lease:
            return lease[0].contract_start
        return None

    @property
    def primary_lease_contract_end(self):
        lease = self.get_primary_lease()
        if lease:
            return lease[0].contract_end
        return None

    @property
    def primary_lease_term(self):
        lease = self.get_primary_lease()
        if lease:
            return lease[0].lease_term
        return None

    @property
    def primary_lease_monthly_rental(self):
        lease = self.get_primary_lease()
        if lease:
            return lease[0].monthly_rental
        return None

    @property
    def secondary_lease_account_number(self):
        lease = self.get_secondary_lease()
        if lease:
            return lease[0].lease_account_no
        return None

    @property
    def secondary_lease_contract_start(self):
        lease = self.get_secondary_lease()
        if lease:
            return lease[0].contract_start
        return None

    @property
    def secondary_lease_contract_end(self):
        lease = self.get_secondary_lease()
        if lease:
            return lease[0].contract_end
        return None

    @property
    def secondary_lease_term(self):
        lease = self.get_secondary_lease()
        if lease:
            return lease[0].lease_term
        return None

    @property
    def secondary_lease_monthly_rental(self):
        lease = self.get_secondary_lease()
        if lease:
            return lease[0].monthly_rental
        return None

    @property
    def primary_assignee(self):
        for assignee in self.assignments:
            if assignee.is_primary:
                return assignee.employee
        return None

    @property
    def primary_assignment(self):
        for assignment in self.assignments:
            if assignment.is_primary:
                return assignment
        return None

    @property
    def effectivity_period(self):
        return '%s TO %s' % (
            self.effectivity_period_start.strftime('%B %d, %Y'),
            self.effectivity_period_end.strftime('%B %d, %Y'))


    @property
    def ordered_rm(self):
        rms = []
        blank_dates = []
        for item in self.vehicle_rm_records:
            if item.date_requested:
                rms.append(item)
            else:
                blank_dates.append(item)
        rms.sort(key=lambda x: x.date_requested, reverse=True)
        return rms + blank_dates


    @property
    def pm_schedule(self):
        pm_schedule = None
        format_make_model = ' '.join(self.make_model.split(' ')[:2])
        if "TOYOTA COROLLA ALTIS" in self.make_model:
            format_make_model = ' '.join([self.make_model.split(' ')[0], self.make_model.split(' ')[2]])
        pm_schedule = PMSchedule.query.filter(
            PMSchedule.unit.ilike('%' + format_make_model + '%')
        ).first()

        # pm_schedule will return none if no Plate is available
        if pm_schedule:
            primary_lease = self.get_primary_lease()
            if not primary_lease:
                primary_lease = self.get_secondary_lease()
            try:
                contract_start = primary_lease[0].contract_start
                first_pm = contract_start + relativedelta(
                    months=pm_schedule.interval)
                second_pm = first_pm + relativedelta(
                    months=1)
                third_pm = second_pm + relativedelta(
                    months=1)
                # if self.last_pm.date_requested:
                #     first_pm = self.last_pm.date_requested + relativedelta(
                #         months=self.pm_schedule['details'].interval
                #     )
                # if self.last_pm.date_requested:
                #     second_pm = self.last_pm.date_requested + relativedelta(
                #         months=self.pm_schedule['details'].interval
                #     )
                #     third_pm = second_pm + relativedelta(
                #         months=self.pm_schedule['details'].interval
                #     )


            except Exception, ex:
                first_pm = None
                second_pm = None

            pm_schedule_dict = {
                'details': pm_schedule,
                'first_pm': first_pm,
                'second_pm': second_pm,
                # 'third_pm': third_pm,
            }

            return pm_schedule_dict
        return pm_schedule


    @property
    def pms(self):
        pm_schedule = None
        format_make_model = ' '.join(self.make_model.split(' ')[:2])
        if "TOYOTA COROLLA ALTIS" in self.make_model:
            format_make_model = ' '.join([self.make_model.split(' ')[0], self.make_model.split(' ')[2]])
        pm_schedule = PMSchedule.query.filter(
            PMSchedule.unit.ilike('%' + format_make_model + '%')
        ).first()

        if pm_schedule:
            primary_lease = self.get_primary_lease()
            if not primary_lease:
                primary_lease = self.get_secondary_lease()
            try:
                contract_start = primary_lease[0].contract_start
                first_pm = contract_start + relativedelta(
                    months=pm_schedule.interval + 1)
                second_pm = first_pm + relativedelta(
                    months=1)
                third_pm = second_pm + relativedelta(
                    months=1)
                try:
                    if self.last_pm:
                        first_pm = self.last_pm.date_requested + relativedelta(
                            months=self.pm_schedule['details'].interval
                        )
                        second_pm = first_pm + relativedelta(
                            months=1
                        )
                        third_pm = second_pm + relativedelta(
                            months=1
                        )
                except:
                    pass
            except Exception, ex:
                first_pm = None
                second_pm = None
                third_pm = None

            pm_schedule_dict = {
                'details': pm_schedule,
                'first_pm': first_pm,
                'second_pm': second_pm,
                'third_pm': third_pm,
            }

            return pm_schedule_dict
        return pm_schedule

    @property
    def notifications(self):
        notifications_dict = {}

        lease = self.get_primary_lease()
        if not lease:
            lease = self.get_secondary_lease()

        # lto
        if len(self.plate_no) >= 6 and self.plate_no[-1] != 'E' \
                and self.plate_no[-1].isdigit():
            ending_number = int(self.plate_no[-1])
            if ending_number == 0:
                ending_number = 10
            if lease:
                start = lease[0].contract_start
                if start:
                    two_years = start + relativedelta(years=2)
                    to_add = 12 - (two_years.month - ending_number + 1)
                    if to_add > 12:
                        to_add -= 12
                    lto_reg = two_years + relativedelta(
                        months=to_add)
                    lto_reg = date(lto_reg.year, ending_number, 1)
                    lto_reg = lto_reg - relativedelta(months=1)
                    notifications_dict['lto_reg'] = lto_reg

        # PMs and battery
        if lease:
            start = lease[0].contract_start
            n_end = lease[0].contract_end
            notifications_dict['start'] = start
            notifications_dict['end'] = n_end

            # First PMS
            try:
                notifications_dict['first_pm_due'] = start + relativedelta(months=1)
            except TypeError:
                pass

            # Second PMS
            try:
                notifications_dict['second_pm_due'] = self.pm_schedule['second_pm']
            except TypeError:
                pass


            # battery
            try:
                two_years = start + relativedelta(years=2)
                notifications_dict['battery'] = two_years
            except TypeError:
                pass

        # lease expirations
        try:
            primary_lease = self.get_primary_lease()
            secondary_lease = self.get_secondary_lease()

            if primary_lease:
                end = primary_lease[0].contract_end - relativedelta(months=3)
                notifications_dict['primary_lease_expiration'] = end

            if secondary_lease:
                end = secondary_lease[0].contract_end - relativedelta(months=3)
                notifications_dict['secondary_lease_expiration'] = end
        except Exception, ex:
            pass

        return notifications_dict

    @property
    def last_pm(self):
        last_record = None
        for record in self.vehicle_rm_records:
            if record.date_requested:
                if last_record is None and \
                        'Periodic Maintenance' in [x.name for x in record.repair_types]:
                    last_record = record
                elif last_record:
                    if record.date_requested > last_record.date_requested and \
                            'Periodic Maintenance' in [x.name for x in record.repair_types]:
                        last_record = record
        return last_record

    @property
    def next_pm(self):
        next_pm = None
        if self.last_pm is not None:
            try:
                # accomplished PM between first and second
                if self.pm_schedule['first_pm'] <= self.last_pm.date_requested \
                        <= self.pm_schedule['second_pm']:
                    next_pm = self.last_pm.date_requested + relativedelta(months=self.pm_schedule['details'].interval)
                elif self.last_pm.date_requested > self.pm_schedule['second_pm']:
                    next_pm = self.last_pm.date_requested + relativedelta(months=self.pm_schedule['details'].interval)
            except:
                return next_pm
        else:
            try:
                next_pm = self.notifications['first_pm_due']
            except:
                return None
        return next_pm

    @property
    def ctpl_insurance(self):
        amount = 0
        for record in self.insurance_record:
            if record.insurance_type.name == 'CTPL':
                amount = amount + record.amount
        return amount


    @property
    def ctpl_insurance_policyno(self):
        for record in self.insurance_record:
            if record.insurance_type.name == 'CTPL':
                return record.oac_policy_no


    @property
    def ctpl_insurance_effectivity_period(self):
        for record in self.insurance_record:
            if record.insurance_type.name == 'CTPL':
                return '%s TO %s' % (
                    record.effectivity_period_start.strftime('%B %d, %Y'),
                    record.effectivity_period_end.strftime('%B %d, %Y'))


    @property
    def comprehensive_insurance(self):
        amount = 0
        for record in self.insurance_record:
            if record.insurance_type.name == 'Comprehensive':
                if record.amount:
                    amount = amount + record.amount
        return amount


    @property
    def comprehensive_insurance_policyno(self):
        for record in self.insurance_record:
            if record.insurance_type.name == 'Comprehensive':
                return record.oac_policy_no


    @property
    def comprehensive_insurance_effectivity_period(self):
        for record in self.insurance_record:
            if record.insurance_type.name == 'Comprehensive':
                return '%s TO %s' % (
                    record.effectivity_period_start.strftime('%B %d, %Y'),
                    record.effectivity_period_end.strftime('%B %d, %Y'))


    def __repr__(self):
        return '%s [%s] (%s)' % (
            self.make_model, self.plate_no, self.cs_no)


class Dealer(db.Model):
    __tablename__ = 'dealers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    city = db.Column(db.String)
    address = db.Column(db.Text)
    telephone_nos = db.Column(db.String)
    service_telephone = db.Column(db.String)
    contact_person = db.Column(db.String)
    email_address = db.Column(db.String)
    fax_number = db.Column(db.String)
    mobile_no = db.Column(db.String)

    def __repr__(self):
        return self.name


class LeasingInformation(db.Model):
    __tablename__ = 'leasing_information'
    id = db.Column(db.Integer, primary_key=True)

    vehicle_id = db.Column(
        db.Integer,
        db.ForeignKey('vehicles.id', ondelete='CASCADE'),
    )
    vehicle = db.relationship('Vehicle', backref='leasing_information',
                              cascade="delete, delete-orphan",
                              single_parent=True)

    lease_account_no = db.Column(db.String(100), nullable=True)
    is_primary = db.Column(db.Boolean, default=True)
    contract_start = db.Column(db.Date)
    contract_end = db.Column(db.Date)
    lease_term = db.Column(db.Integer, nullable=False)
    monthly_rental = db.Column(db.Numeric(8, 2))

    __table_args__ = (
        db.UniqueConstraint('vehicle_id', 'is_primary'),
    )


class AccidentNature(GenericLookupTable):
    __tablename__ = 'accident_nature'


class Incident(db.Model):
    __tablename__ = 'incidents'
    id = db.Column(db.Integer, primary_key=True)
    fleet_incident_id = db.Column(db.String, unique=True, nullable=True)

    assignment_id = db.Column(
        db.Integer,
        db.ForeignKey('vehicles_users.id', ondelete='CASCADE')
    )
    assignment = db.relationship('VehiclesToEmployees',
                                 backref='assignment_incidences')

    designated_driver_id = db.Column(
        db.Integer,
        db.ForeignKey('designated_drivers.id', ondelete='SET NULL')
    )
    designated_driver = db.relationship('DesignatedDriver',
                                        backref='driver_incidences')

    date = db.Column(db.Date, nullable=False)

    accident_nature_id = db.Column(
        db.Integer,
        db.ForeignKey('accident_nature.id', ondelete='CASCADE'),
        nullable=False
    )
    accident_nature = db.relationship('AccidentNature',
                                      backref='incidences_by_nature')

    location = db.Column(db.Text, nullable=False)
    details = db.Column(db.Text)
    damaged_portion = db.Column(db.Text)
    loa = db.Column(db.Text)
    repair_shop = db.Column(db.Text)
    repair_cost = db.Column(db.Float(precision=10, asdecimal=True,
                                     decimal_return_scale=2))
    remarks = db.Column(db.Text)
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(),
                           onupdate=db.func.now())

    is_claimable = db.Column(db.Boolean, default=False)

    _mapper_args__ = {
        "order_by": fleet_incident_id
    }


class RepairTypesToRM(db.Model):
    __tablename__ = 'repair_types_rm'
    rm_id = db.Column(db.Integer,
                      db.ForeignKey('repairs_maintenance.id',
                                    ondelete='CASCADE'),
                      primary_key=True)
    rm = db.relationship('RepairMaintenance')

    repair_type_od = db.Column(db.Integer,
                               db.ForeignKey('repair_types.id',
                                             ondelete='CASCADE'),
                               primary_key=True)
    repair_type = db.relationship('RepairType')


class RepairMaintenance(db.Model):
    __tablename__ = 'repairs_maintenance'
    id = db.Column(db.Integer, primary_key=True)
    date_requested = db.Column(db.Date)
    km_reading = db.Column(db.Integer, nullable=True)
    activity_concern = db.Column(db.Text)
    cost_estimate = db.Column(db.Float(precision=10, asdecimal=True,
                                       decimal_return_scale=2),)
    actual_amount = db.Column(db.Float(precision=10, asdecimal=True,
                                       decimal_return_scale=2),)
    invoice_no = db.Column(db.String)
    remarks = db.Column(db.Text)

    vehicle_id = db.Column(
        db.Integer,
        db.ForeignKey('vehicles.id', ondelete='SET NULL'),
        nullable=False)

    vehicle = db.relationship('Vehicle', backref='vehicle_rm_records')

    employee_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='SET NULL'),
        nullable=True
    )

    employee = db.relationship('Employee', backref='rm_records')

    repair_types = db.relationship(
        'RepairType',
        secondary=RepairTypesToRM.__tablename__,
        backref='rm_record'
    )

    dealer_id = db.Column(
        db.Integer,
        db.ForeignKey('dealers.id', ondelete='CASCADE'),
    )
    dealer = db.relationship('Dealer')

    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(),
                           onupdate=db.func.now())

    # __mapper_args__ = {
    #     "order_by": "date_requested desc NULLS last"
    # }


class RepairType(GenericLookupTable):
    __tablename__ = 'repair_types'


class PMSchedule(db.Model):
    __tablename__ = 'pm_schedule'
    id = db.Column(db.Integer, primary_key=True)
    unit = db.Column(db.String, nullable=False, unique=True)
    first_pm = db.Column(db.Integer, nullable=False)
    second_pm = db.Column(db.Integer, nullable=False)
    interval = db.Column(db.Integer, nullable=False)


class EmailTemplate(db.Model):
    __tablename__ = 'email_templates'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=True)
    poster = db.Column(db.String, nullable=True)
    template = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return self.name


class EmailTemplateAttachment(db.Model):
    __tablename__ = 'email_template_attachments'
    id = db.Column(db.Integer, primary_key=True)
    email_template_id = db.Column(
        db.Integer,
        db.ForeignKey('email_templates.id', ondelete='SET NULL'),
        nullable=False
    )
    email_template = db.relationship("EmailTemplate",
                                     backref='email_template_attachments')
    attachment = db.Column(db.String, nullable=True)



class EmailNotification(db.Model):
    __tablename__ = 'email_notifications'
    id = db.Column(db.Integer, primary_key=True)
    email_template_id = db.Column(
        db.Integer,
        db.ForeignKey('email_templates.id', ondelete='SET NULL'),
        nullable=True
    )
    email_template = db.relationship('EmailTemplate',
                                     backref='notifications_sent')
    recipient_id = db.Column(
        db.Integer,
        db.ForeignKey('employees.id', ondelete='SET NULL'),
        nullable=True
    )
    recipient = db.relationship('Employee',
                                backref='emails_received')
    date_sent = db.Column(db.DateTime, server_default=db.func.now())


class InsuranceType(db.Model):
    __tablename__ = 'insurance_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)

    def __repr__(self):
        return self.name


class Insurance(db.Model):
    __tablename__ = 'insurance'
    id = db.Column(db.Integer, primary_key=True)
    oac_policy_no = db.Column(db.String(50))
    effectivity_period_start = db.Column(db.Date)
    effectivity_period_end = db.Column(db.Date)
    vehicle_id = db.Column(
        db.Integer,
        db.ForeignKey('vehicles.id', ondelete='CASCADE'),
        nullable=False
    )
    vehicle = db.relationship('Vehicle', backref='insurance_record')
    insurance_type_id = db.Column(
        db.Integer,
        db.ForeignKey('insurance_types.id', ondelete='SET NULL'),
        nullable=True
    )
    insurance_type = db.relationship('InsuranceType', backref='insurance_records_from_types')
    amount = db.Column(db.Numeric(10, 2))

    def __repr__(self):
        return '%s - %s - %s' % (self.vehicle.engine_no, self.insurance_type.name, str(self.amount))

class NotificationSwitch(db.Model):
    __tablename__ = 'notification_switch'
    id = db.Column(db.Integer, primary_key=True)
    on_off = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return str(self.on_off)


class NotificationLog(db.Model):
    __tablename__ = 'notification_logs'
    id = db.Column(db.Integer, primary_key=True)
    recipient = db.Column(db.String(255), nullable=True)
    time_sent = db.Column(db.DateTime, server_default=db.func.now())
    notification_type = db.Column(db.String(255), nullable=False)
