from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_login import LoginManager
from flask_mail import Mail
from config import config

import os
import smtplib
from email.mime.text import MIMEText

from dateutil.relativedelta import relativedelta


db = SQLAlchemy()
admin = Admin(template_mode='bootstrap3')
mail = Mail()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    admin.init_app(app)

    login_manager.init_app(app)

    try:
        if os.environ['FMS_CONFIG'] == 'prod':
            app.config['MAIL_SERVER'] = '10.10.1.121'
            app.config['MAIL_PORT'] = 25
            app.config['MAIL_DEFAULT_SENDER'] = 'facilitiesandfleetmanagement@ccac.com.ph'
            app.config['MAIL_DEBUG'] = False
            app.config['MAIL_MAX_EMAILS'] = 100
            mail.init_app(app)
        elif os.environ['FMS_CONFIG'] == 'dev':
            app.config['MAIL_SERVER'] = 'smtp.mailgun.org'
            app.config['MAIL_PORT'] = 587
            app.config['MAIL_USERNAME'] = 'postmaster@mg.stratustec.ph'
            app.config['MAIL_PASSWORD'] = '87defd11459466d2f20e257b32ff3765'
            app.config['MAIL_DEFAULT_SENDER'] = 'facilitiesandfleetmanagement@ccac.com.ph'
            app.config['MAIL_DEBUG'] = False
            mail.init_app(app)
    except:
        pass

    if app.config['DEBUG']:
        from flask_debugtoolbar import DebugToolbarExtension
        toolbar = DebugToolbarExtension()
        toolbar.init_app(app)

    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    @app.template_filter('money')
    def money(value, with_decimal=True):
        if value:
            if with_decimal:
                return "PHP {:,.2f}".format(value)
            return "PHP {:,}".format(int(value))
        else:
            return ""

    @app.template_filter('date_format')
    def date_format(value):
        if value:
            return value.strftime('%B %d, %Y')
        return ""

    @app.template_filter('date_format_lto')
    def date_format_lto(value):
        if value:
            return (value + relativedelta(years=3)).strftime('%B %d, %Y')
        return ""

    @app.template_filter('date_format_battery')
    def date_format_battery(value):
        if value:
            return (value + relativedelta(years=2)).strftime('%B %d, %Y')
        return ""

    return app
