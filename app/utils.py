from . import db
from .models import Vehicle
from datetime import date
from collections import OrderedDict

import calendar


ytd = date.today().year
active_vehicle_query = db.session.query(Vehicle).filter(Vehicle.status == 'ACTIVE')
months = [x for x in calendar.month_name]


def get_first_pm():
    pass


def get_second_pm():
    pass


def get_lto_reg():
    pass


def get_battery_replacement():
    pass


def get_primary_lease_expiration():
    months_dict = OrderedDict(months)
    print months_dict


def get_secondary_lease_expiration():
    pass


if __name__ == '__main__':
    get_primary_lease_expiration()
    