// Plugins
var gulp = require('gulp');
var pjson = require('./package.json');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var del = require('del');
var plumber = require('gulp-plumber');
var pixrem = require('gulp-pixrem');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var exec = require('child_process').exec;
var merge = require('merge-stream');
var minify = require('gulp-minify-css');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var pathsConfig = function(appName) {
  this.app = "./" + (appName || pjson.name);

  return {
    app: this.app,
    src: this.app + '/static/src',
    templates: this.app + '/templates',
    sass: this.app + '/static/src/sass',
    fonts: this.app + '/static/src/fonts',
    images: this.app + '/static/src/images',
    js: this.app + '/static/src/js',
    components: this.app + '/static/src/bower_components',
    dist: {
      js: this.app + '/static/dist/js',
      css: this.app + '/static/dist/css',
      images: this.app + '/static/dist/images',
      fonts: this.app + '/static/dist/fonts/',
    }
  }
};

var sourcesConfig = function (paths) {
  return {
    js: [
      // add 3rd party js here
      paths.components + '/jquery/dist/jquery.min.js',
      paths.components + '/bootstrap/dist/js/bootstrap.min.js',
      paths.components + '/moment/moment.js',
      paths.js + '**/*.js'
    ],
    sass: [
      // paths.src + '/paper-dashboard.scss'
    ],
    css: [
      paths.components + '/font-awesome/css/font-awesome.min.css',
      paths.components + '/bootstrap/dist/css/bootstrap.min.css',
      paths.src + '/custom.css',
      paths.components + '/bootstrap/dist/css/bootstrap-theme.min.css',
    ],
    images: [
      paths.images + '/*'
    ],
    fonts: [
      paths.components + '/bootstrap/fonts/glyphicons-halflings-regular.*',
      paths.components + '/font-awesome/fonts/fontawesome-webfont.*'
    ]
  }
};

var paths = pathsConfig('app');
var sources = sourcesConfig(paths);

/**
 * TASKS
 */
gulp.task('clean', function() {
  return del([paths.app + '/static/dist']);
});

gulp.task('styles', function() {

  var scssStream = gulp.src(sources.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(plumber())
    .pipe(autoprefixer({browsers: ['last 2 version']}))
    .pipe(pixrem())
    .pipe(concat('scss-files.css'))

  var cssStream = gulp.src(sources.css)
    .pipe(concat('css-files.css'))

  var mergedStream = merge(scssStream, cssStream)
    .pipe(concat('styles.css'))
    .pipe(minify())
    .pipe(gulp.dest(paths.dist.css));

  // return gulp.src(sources.sass)
  //   .pipe(sass().on('error', sass.logError))
  //   .pipe(plumber())
  //   .pipe(autoprefixer({browsers: ['last 2 version']}))
  //   .pipe(pixrem())
  //   .pipe(gulp.dest(paths.dist.css))
  //   .pipe(rename('app.min.css'))
  //   .pipe(cssnano())
  //   .pipe(gulp.dest(paths.dist.css));
});

gulp.task('scripts', function() {
  return gulp.src(sources.js)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest(paths.dist.js))
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(paths.dist.js));
});

gulp.task('images', function() {
  return gulp.src(sources.images)
    // .pipe(imagemin())
    .pipe(gulp.dest(paths.dist.images))
});

gulp.task('fonts', function() {
  return gulp.src(sources.fonts)
    .pipe(gulp.dest(paths.dist.fonts))
})

gulp.task('runServer', function() {
  exec('venv/bin/python manage.py runserver', function(err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});

gulp.task('browserSync', function() {
  browserSync.init(
    [paths.dist.css + "/*.css", paths.dist.js + "*.js", paths.templates + '*.html'],
    {
      proxy: "localhost:5000"
    }
  );
});

gulp.task('watch', function() {
  gulp.watch(paths.sass + '/**/*.scss', ['styles']).on('change', reload);
  gulp.watch(paths.js + '/**/*.js', ['scripts']).on("change", reload);
  gulp.watch(paths.images + '/*', ['images']);
  gulp.watch(paths.templates + '/**/*.html').on("change", reload);
});

// Build and compile all files
gulp.task('build', function () {
  runSequence('clean', 'styles', 'scripts', 'images', 'fonts');
});

// Build all files and watches for changes
gulp.task('build:watch', ['build', 'watch']);

// Build all files, run the server, start BrowserSync and watch for file changes
gulp.task('default', function () {
  runSequence('build', 'runServer', 'browserSync', 'watch');
});

// Build all files, start BrowserSync and watch for file changes (use it when you want to start Flask manually)
gulp.task('dev', function () {
  runSequence('build', 'browserSync', 'watch');
});

