import unittest
from app import create_app, db
from app.models import Employee,\
    EmployeeClassification as Classification,\
    EmployeeDepartment as Department,\
    EmployeeSBU as SBU,\
    EmployeeCostCenter as CostCenter


class EmployeeModelsTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_classification(self):
        c = Classification(name='Test Classification')
        db.session.add(c)
        db.session.commit()
        self.assertTrue(isinstance(c, Classification))

    def test_create_department(self):
        d = Department(name='Test Department')
        db.session.add(d)
        db.session.commit()
        self.assertTrue(isinstance(d, Department))

    def test_create_employee(self):
        e = Employee(
            first_name='Jino',
            last_name='Raymundo',
            email='jinoraymundo@gmail.com'
        )
        db.session.add(e)
        db.session.commit()

        self.assertTrue(isinstance(e, Employee))
        self.assertIsNone(e.suffix)
        self.assertIsNone(e.department)
        self.assertIsNone(e.classification)

    def test_basic_employee(self):
        c = Classification(name='Test Classification')
        d = Department(name='Test Department')
        sbu = SBU(name='Test SBU')
        cc = CostCenter(name='Test Cost Center')
        e = Employee(
            first_name='Jino',
            last_name='Raymundo',
            email='jinoraymundo@gmail.com'
        )
        db.session.add(c)
        db.session.add(d)
        db.session.add(sbu)
        db.session.add(cc)
        db.session.add(e)
        db.session.commit()

        self.assertIsNone(e.suffix)
        self.assertIsNone(e.department)
        self.assertIsNone(e.classification)

        e.department = d
        e.classification = c
        e.SBU = sbu
        e.cost_center = cc
        db.session.add(e)
        db.session.commit()

        self.assertEqual(e.department, d)
        self.assertEqual(e.classification, c)
        self.assertEqual(e.SBU, sbu)
        self.assertEqual(e.cost_center, cc)

        db.session.delete(d)
        db.session.delete(c)
        db.session.delete(sbu)
        db.session.delete(cc)
        db.session.commit()

        self.assertIsNone(e.department)
        self.assertIsNone(e.classification)
        self.assertIsNone(e.cost_center)
        self.assertIsNone(e.SBU)
