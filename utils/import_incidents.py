import os
import csv

from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from app.models import Incident, Vehicle, Employee, VehiclesToEmployees,\
    DesignatedDriver, AccidentNature


base_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
data_path = os.path.join(base_path, 'data', 'fleet_incidents')


def find_vehicle(plate_or_cs_no):
    vehicle = Vehicle.query.filter_by(plate_no=plate_or_cs_no).one_or_none()
    if not vehicle:
        vehicle = Vehicle.query.filter_by(cs_no=plate_or_cs_no).one_or_none()
        if not vehicle:
            return None
    return vehicle


def find_employee(employee_name):
    names_dict = {
        'employee': None,
    }
    if employee_name != "Not in Employee DB" and employee_name is not None and employee_name != "":
        name = employee_name.strip()
        print name
        last_name = name.split(', ')[0].lower().capitalize()
        first_name = name.split(', ')[1].split(' ')[0].lower().capitalize()

        try:
            employee = Employee.query.\
                filter(Employee.last_name == last_name).\
                filter(Employee.first_name.like('%' + first_name + '%')).\
                one_or_none()
            names_dict['employee'] = employee
        except MultipleResultsFound:
            print "MULTIPLE", name

    return names_dict


def find_driver(name):
    try:
        driver = DesignatedDriver.query.filter(
            DesignatedDriver.name.like('%' + name + '%')
        ).one_or_none()
        return driver
    except Exception:
        return None
    return None


def find_accident_class(name):
    try:
        accident_class = AccidentNature.query.filter_by(
            name=name).one_or_none()
        return accident_class
    except Exception:
        return None
    return None


def log_errors(row, year, error):
    errorcsv_file_path = os.path.join(data_path, '%s_errors.csv' % year)
    with open(errorcsv_file_path, 'a') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_ALL)
        csvwriter.writerow([error] + row)


def populate_incidences(app, db, year):
    csv_file_path = os.path.join(data_path, '%s.csv' % year)
    with open(csv_file_path, 'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        for item in csvreader:
            print item[0]
            vehicle = find_vehicle(item[1])
            employee = find_employee(item[2])

            if vehicle is None:
                log_errors(item, year, 'Vehicle not found')
                continue

            if employee['employee'] is None:
                log_errors(item, year, 'Employee not found')
                continue

            assignment = VehiclesToEmployees.query.filter_by(
                employee=employee['employee'],
                vehicle=vehicle
            ).one_or_none()

            repair_cost = None
            if item[10]:
                repair_cost = float(item[10])

            if not assignment:
                log_errors(item, year,
                           'Vehicle to Employee assignment not found')
                continue

            incident_dict = {
                'fleet_incident_id': item[0],
                'assignment': assignment,
                'date': item[3],
                'location': item[4],
                'accident_nature': find_accident_class(item[5]),
                'details': item[6],
                'damaged_portion': item[7],
                'repair_shop': item[8],
                'loa': item[9],
                'repair_cost': repair_cost,
                'remarks': item[11],
            }

            incident = Incident(**incident_dict)
            db.session.add(incident)
            db.session.commit()
