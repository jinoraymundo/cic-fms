import os
import csv
from app.models import Employee,\
    EmployeeDepartment as Department,\
    EmployeeClassification as Classification,\
    EmployeeSBU as SBU,\
    EmployeeCostCenter as CostCenter,\
    EmployeeBU as BU,\
    EmployeeLocation as Location,\
    EmployeePosition as Position,\
    EmployeeTOT as TOT
from app.models import Role, User

from .import_incidents import populate_incidences
from .import_rms import populate_randms

BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


def populate_SBU(db, rows):
    SBUs = [SBU(name=x[0]) for x in rows]
    db.session.bulk_save_objects(SBUs)
    print "Done populating SBUs"


def populate_BU(db, rows):
    BUs = [BU(name=x[0]) for x in rows]
    db.session.bulk_save_objects(BUs)
    print "Done populating BUs"


def populate_cost_center(db, rows):
    cost_centers = [CostCenter(name=x[0]) for x in rows]
    db.session.bulk_save_objects(cost_centers)
    print "Done populating Cost Centers"


def populate_classification(db, rows):
    classifications = [Classification(name=x[0]) for x in rows]
    db.session.bulk_save_objects(classifications)
    print "Done populating Classification Codes"


def populate_department(db, rows):
    departments = [Department(name=x[0]) for x in rows]
    db.session.bulk_save_objects(departments)
    print "Done populating departments"


def populate_location(db, rows):
    locations = [Location(name=x[0]) for x in rows]
    db.session.bulk_save_objects(locations)
    print "Done populating locations"


def populate_position(db, rows):
    positions = [Position(name=x[0]) for x in rows]
    db.session.bulk_save_objects(positions)
    print "Done populating positions"


def populate_TOT(db, rows):
    TOTs = [TOT(name=x[0]) for x in rows]
    db.session.bulk_save_objects(TOTs)
    print "Done populating TOTs"


def populate_employee(db, rows):
    employees = []
    for row in rows:
        last_name, first_name = row
        suffix = ""
        if 'Jr.' in last_name:
            suffix = 'Jr.'
            last_name = last_name[:-4]
        employee = Employee(first_name=first_name.strip(),
                            last_name=last_name.strip(),
                            suffix=suffix.strip())
        employees.append(employee)
    db.session.bulk_save_objects(employees)
    print "Done populating employees"


def populate_roles(db, rows):
    roles = [Role(name=x[0]) for x in rows]
    db.session.bulk_save_objects(roles)
    print "Done populating roles"


def populate_users(db, rows):
    users = [User(email=x[0], password=x[1], active=True) for x in rows]
    db.session.bulk_save_objects(users)
    print "Done populating users"


def populate_employee_related_models(app, db):

    db.drop_all()
    db.create_all()

    data_path = os.path.abspath(
        os.path.join(BASE_PATH, 'data'))
    csv_files = sorted(os.listdir(data_path))

    for csv_file in csv_files:
        model_name = csv_file[:-4]

        with open(os.path.join(data_path, csv_file), 'rb') as csvfile:
            csvreader = csv.reader(csvfile)
            if model_name == 'employee_SBUs':
                populate_SBU(db, csvreader)
            if model_name == 'employee_cost_centers':
                populate_cost_center(db, csvreader)
            if model_name == 'employee_classifications':
                populate_classification(db, csvreader)
            if model_name == 'employee_departments':
                populate_department(db, csvreader)
            if model_name == 'employee_BUs':
                populate_BU(db, csvreader)
            if model_name == 'employee_locations':
                populate_location(db, csvreader)
            if model_name == 'employee_positions':
                populate_position(db, csvreader)
            if model_name == 'employee_TOTs':
                populate_TOT(db, csvreader)
            if model_name == 'employees':
                populate_employee(db, csvreader)
            if model_name == 'roles':
                populate_roles(db, csvreader)
            if model_name == 'users':
                populate_users(db, csvreader)
