import os
import csv

from sqlalchemy.exc import DataError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
import datetime


from app.models import RepairMaintenance, Employee, Vehicle, Dealer


base_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
data_path = os.path.join(base_path, 'data', 'repairs_maintenance')


def find_vehicle(plate_or_cs_no):
    vehicle = Vehicle.query.filter_by(plate_no=plate_or_cs_no).one_or_none()
    if not vehicle:
        vehicle = Vehicle.query.filter_by(cs_no=plate_or_cs_no).one_or_none()
        if not vehicle:
            return None
    return vehicle


def find_employee(employee_name):
    value = None
    if employee_name != "Not in Employee DB" and employee_name is not None and employee_name != "":
        try:
            name = employee_name.strip()
            last_name = name.split(', ')[0]
            first_name = name.split(', ')[1].split(' ')[0]
        except Exception:
            return None

        try:
            employee = Employee.query.\
                filter(Employee.last_name == last_name).\
                filter(Employee.first_name.like('%' + first_name + '%')).\
                one_or_none()
            value = employee
        except MultipleResultsFound:
            print "MULTIPLE", name

    return value


def find_dealer(dealer_name):
    dealer = Dealer.query.filter_by(name=dealer_name).one_or_none()
    return dealer


def log_errors(row, error):
    errorcsv_file_path = os.path.join(data_path, 'rms_errors.csv')
    with open(errorcsv_file_path, 'a') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_ALL)
        csvwriter.writerow([error] + row)


def populate_randms(app, db):
    csv_file_path = os.path.join(data_path, 'rms.csv')
    with open(csv_file_path, 'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        for item in csvreader:
            vehicle = find_vehicle(item[0])
            if not vehicle:
                log_errors(item, "Vehicle not found")
                continue

            date_serviced = None
            if item[6] and item[6] != "-" and item[6] != "/":
                try:
                    date_serviced = datetime.datetime.strptime(item[6], '%d/%m/%Y').strftime('%m/%d/%Y')
                    date_serviced = item[6]
                except:
                    date_serviced = datetime.datetime.strptime(item[6], '%m/%d/%Y').strftime('%m/%d/%Y')
                    date_serviced = item[6]


            km_reading = None
            if item[8]:
                km_reading = int(item[8])

            cost_estimate = None
            if item[4]:
                try:
                    cost_estimate = float(item[4])
                except ValueError:
                    pass

            actual_amount = None
            if item[5]:
                try:
                    actual_amount = float(item[5])
                except ValueError:
                    pass

            randm_dict = dict(
                vehicle=vehicle,
                employee=find_employee(item[1]),
                date_requested=date_serviced,
                activity_concern=item[3],
                cost_estimate=cost_estimate,
                actual_amount=actual_amount,
                dealer=find_dealer(item[2]),
                invoice_no=item[7],
                km_reading=km_reading,
            )

            try:
                randm = RepairMaintenance(**randm_dict)
            except DataError:
                log_errors(item, "Invalid Date")
                continue
