#!/usr/bin/env python
import os
import csv
from app import create_app, db
from app.models import Employee,\
    EmployeeEntity as Entity,\
    EmployeeDepartment as Department,\
    EmployeeSection as Section,\
    EmployeeClassificationOther as ClassificationOther,\
    EmployeeClassification as Classification,\
    EmployeeSBU as SBU,\
    EmployeeCostCenter as CostCenter,\
    EmployeeBU as BU,\
    EmployeeLocation as Location,\
    EmployeePosition as Position,\
    EmployeeTOT as TOT,\
    Vehicle, VehiclesToEmployees,\
    RepairMaintenance, RepairType, RepairTypesToRM,\
    Incident,\
    PMSchedule,\
    InsuranceType, Insurance
from app.models import User, Role
from app.notification_scans import scan_all
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from utils import populate_employee_related_models, populate_incidences,\
    populate_randms

from sqlalchemy import exc
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from datetime import datetime


BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
DATA_PATH = os.path.join(BASE_PATH, 'data')


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    return dict(app=app, db=db, Employee=Employee, Department=Department,
                Classification=Classification, SBU=SBU, CostCenter=CostCenter,
                BU=BU, Location=Location, Position=Position, TOT=TOT,
                User=User, Role=Role, Vehicle=Vehicle, Incident=Incident)


manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)
manager.add_option('-c', '--config', dest='config', required=False)


@manager.command
def populate_db_from_spreadsheet():
    populate_employee_related_models(app, db)


@manager.command
def populate_entities():
    csv_file = os.path.join(DATA_PATH, 'employee_entities.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        entities = [Entity(name=x[0]) for x in rows]
        db.session.bulk_save_objects(entities)


@manager.command
def populate_classifications():
    csv_file = os.path.join(DATA_PATH, 'employee_classifications_other.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        classifications = [ClassificationOther(name=x[0]) for x in rows]
        db.session.bulk_save_objects(classifications)


@manager.command
def populate_departments():
    csv_file = os.path.join(DATA_PATH, 'employee_departments.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        departments = [Department(name=x[0]) for x in rows]
        for department in departments:
            try:
                db.session.add(department)
                db.session.commit()
            except exc.IntegrityError:
                print 'already existing: %s' % department.name
                db.session.rollback()


@manager.command
def populate_sections():
    csv_file = os.path.join(DATA_PATH, 'employee_sections.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        sections = [Section(name=x[0]) for x in rows]
        db.session.bulk_save_objects(sections)


@manager.command
def populate_BUs():
    csv_file = os.path.join(DATA_PATH, 'employee_BUs.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        BUs = [BU(name=x[0]) for x in rows]
        for bu in BUs:
            try:
                db.session.add(bu)
                db.session.commit()
            except exc.IntegrityError:
                print "already existing: %s" % bu.name
                db.session.rollback()


@manager.command
def populate_positions():
    csv_file = os.path.join(DATA_PATH, 'employee_positions.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        positions = [Position(name=x[0]) for x in rows]
        for position in positions:
            try:
                db.session.add(position)
                db.session.commit()
            except exc.IntegrityError:
                print "already existing: %s" % position.name
                db.session.rollback()


@manager.command
def populate_cost_centers():
    csv_file = os.path.join(DATA_PATH, 'employee_cost_centers.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        cost_centers = [CostCenter(name=x[0]) for x in rows]
        for cost_center in cost_centers:
            try:
                db.session.add(cost_center)
                db.session.commit()
            except exc.IntegrityError:
                print "already existing: %s" % cost_center.name
                db.session.rollback()


@manager.command
def populate_employee_master():
    csv_file = os.path.join(DATA_PATH, 'employees_master.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        index = 0
        for row in rows:
            print index, row[3]
            middle_name = ""
            if row[7] != "-" or row[7] != "":
                middle_name = row[7]

            doh = datetime.strptime(row[10], '%d-%b-%y')

            if row[1]:
                sbu = db.session.query(SBU).filter_by(name=row[1]).one()
            else:
                sbu = None

            if row[2]:
                bu = db.session.query(BU).filter_by(name=row[2]).one()
            else:
                bu = None

            if row[3]:
                department = db.session.query(Department).filter_by(name=row[3]).one()
            else:
                deparment = None

            if row[4]:
                section = db.session.query(Section).filter_by(name=row[4]).one()
            else:
                section = None

            if row[5]:
                cost_center = db.session.query(CostCenter).filter_by(name=row[5]).one()
            else:
                cost_center = None

            if row[6]:
                classification = db.session.query(ClassificationOther).filter_by(name=row[11]).one()
            else:
                classification = None

            if row[12]:
                position = db.session.query(Position).filter_by(name=row[12]).one()
            else:
                position = None

            if row[13]:
                classification_code = db.session.query(Classification).filter_by(name=row[13]).one()
            else:
                classification_code = None


            employee = Employee(
                first_name=row[6],
                last_name=row[8],
                middle_name=middle_name,
                doh=datetime.strftime(doh, '%Y-%m-%d'),
                gender=row[14],
                entity=db.session.query(Entity).filter_by(name=row[0]).one(),
                SBU=sbu,
                BU=bu,
                department=department,
                section=section,
                cost_center=cost_center,
                classification=classification,
                position=position,
                classification_code=classification_code
            )

            try:
                in_db = db.session.query(Employee).filter_by(
                    first_name=employee.first_name,
                    last_name=employee.last_name).one()
                in_db.middle_name = employee.middle_name
                in_db.doh = employee.doh
                in_db.gender = employee.gender
                in_db.entity = employee.entity
                in_db.SBU = employee.SBU
                in_db.BU = employee.BU
                in_db.department = employee.department
                in_db.section = employee.section
                in_db.cost_center = employee.cost_center
                in_db.classification = employee.classification
                in_db.position = employee.position
                in_db.classification_code = employee.classification_code

                db.session.add(in_db)
                db.session.commit()
            except Exception:
                print "NEW %s" % employee
                db.session.add(employee)
                db.session.commit()
            index = index + 1


@manager.command
def populate_randm():
    csv_file = os.path.join(DATA_PATH, 'randm.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=";")
        for row in rows:
            try:
                vehicle = Vehicle.query.filter_by(plate_no=row[0].upper()).one()
                # print vehicle, "FROM PLATE"
                pass
            except NoResultFound:
                try:
                    vehicle = Vehicle.query.filter_by(cs_no=row[0].upper()).one()
                    # print vehicle, "FROM CS"
                    pass
                except MultipleResultsFound:
                    # print row[0], "FROM CS#"
                    pass
                except NoResultFound:
                    print row[0], "NO RESULT"
                except UnicodeEncodeError:
                    print row[0]
            except MultipleResultsFound:
                print row[0], "FROM PLATE#"
            except UnicodeEncodeError:
                print "UNICODE", row[0]


@manager.command
def populate_assignments():
    for vehicle in Vehicle.query.all():
        assignment = VehiclesToEmployees(
            employee=vehicle.primary_assignee,
            vehicle=vehicle,
            assigned_start=vehicle.date_assigned,
            is_primary=True)
        db.session.add(assignment)
        db.session.commit()


@manager.command
def migrate_repair_types():
    for rm in RepairMaintenance.query.all():
        if rm.repair_type:
            repair_type = rm.repair_type
            rm.repair_types.append(repair_type)
            db.session.add(rm)
            db.session.commit()


@manager.command
def migrate_incidences():
    for incident in Incident.query.order_by('id').all():
        try:
            assignment = VehiclesToEmployees.query.filter_by(
                employee=incident.employee,
                vehicle=incident.employee
            ).one()
            # print assignment.vehicle, assignment.employee
        except NoResultFound:
            print "No assignment found for",
            print incident.vehicle, incident.employee
    # for incident in Incident.query.order_by('id').all():
        # try:
        #     assignment = VehiclesToEmployees.query.filter_by(
        #         employee=incident.employee,
        #         vehicle=incident.vehicle
        #     ).one()
        #     print assignment.id
        # except NoResultFound:
        #     print assignment.id, incident.id


@manager.command
def test():
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@manager.command
def populate_fleet_incidences(year=None):
    populate_incidences(app, db, year)


@manager.command
def populate_rms():
    populate_randms(app, db)


@manager.command
def populate_pm_schedule():
    csv_file = os.path.join(DATA_PATH, 'pm_schedule.csv')
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        schedules = [PMSchedule(unit=x[0], first_pm=x[1],
                                second_pm=x[2], interval=x[3]) for x in rows]
        db.session.bulk_save_objects(schedules)

@manager.command
def populate_insurance_types():
    insurance_types = [
        InsuranceType(name='Comprehensive'),
        InsuranceType(name='CTPL'),
        InsuranceType(name='Extension-Compre'),
    ]
    db.session.bulk_save_objects(insurance_types)


@manager.command
def populate_insurance():
    csv_file = os.path.join(DATA_PATH, 'insurance_upload.csv')
    insurance_records = []
    with open(csv_file, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        for row in rows:
            insurance_type = InsuranceType.query.filter_by(
                name=row[1],
            ).one()
            vehicle = Vehicle.query.filter_by(
                engine_no=row[0]
            ).one()
            amount = row[2]
            insurance_records.append(
                Insurance(
                    vehicle_id=vehicle.id,
                    insurance_type_id=insurance_type.id,
                    amount=amount)
            )
    db.session.bulk_save_objects(insurance_records)


@manager.command
def notification_scans():
    scan_all(app, db)

if __name__ == "__main__":
    manager.run()
