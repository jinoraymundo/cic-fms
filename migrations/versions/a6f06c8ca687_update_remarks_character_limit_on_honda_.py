"""update remarks character limit on honda certifications

Revision ID: a6f06c8ca687
Revises: c2ff244b928e
Create Date: 2017-04-10 01:37:56.006641

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a6f06c8ca687'
down_revision = 'c2ff244b928e'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('honda_certification', 'remarks',
                    existing_type=sa.String(16),
                    type_=sa.String(64))


def downgrade():
    op.alter_column('honda_certification', 'remarks',
                    existing_type=sa.String(64),
                    type_=sa.String(16))
