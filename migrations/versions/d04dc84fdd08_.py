"""empty message

Revision ID: d04dc84fdd08
Revises: 3f45357efe81
Create Date: 2017-04-02 00:16:39.731248

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd04dc84fdd08'
down_revision = '3f45357efe81'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('contact_numbers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('number', sa.String(length=128), nullable=False),
    sa.Column('employee_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['employee_id'], ['employees.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('contact_numbers')
    # ### end Alembic commands ###
