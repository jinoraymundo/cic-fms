"""made repair_types m2m to R&M

Revision ID: e8ae51b37826
Revises: 610889638cb7
Create Date: 2017-04-22 08:47:26.637719

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e8ae51b37826'
down_revision = '610889638cb7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('repair_types_rm',
    sa.Column('rm_id', sa.Integer(), nullable=False),
    sa.Column('repair_type_od', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['repair_type_od'], ['repair_types.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['rm_id'], ['repairs_maintenance.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('rm_id', 'repair_type_od')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('repair_types_rm')
    # ### end Alembic commands ###
