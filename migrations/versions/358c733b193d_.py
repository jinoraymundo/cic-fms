"""empty message

Revision ID: 358c733b193d
Revises: 1dca3aae2561
Create Date: 2018-01-10 13:17:07.300190

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '358c733b193d'
down_revision = '1dca3aae2561'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('vehicles', 'oac_policy_no',
        existing_type=sa.VARCHAR(length=14),
        type_=sa.VARCHAR(length=50))


def downgrade():
    op.alter_column('vehicles', 'oac_policy_no',
        existing_type=sa.VARCHAR(length=50),
        type_=sa.VARCHAR(length=14))
