"""change acquisition_cost and credit_limit data type on Vehicle

Revision ID: 337831ae00cb
Revises: c552c7baddc4
Create Date: 2017-05-01 19:11:35.330457

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '337831ae00cb'
down_revision = 'c552c7baddc4'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        'vehicles',
        'credit_limit',
        existing_type=sa.Float(),
        type_=sa.Numeric(10, 2),
    )

    op.alter_column(
        'vehicles',
        'acquisition_cost',
        existing_type=sa.Float(),
        type_=sa.Numeric(10, 2),
    )


def downgrade():
    op.alter_column(
        'vehicles',
        'credit_limit',
        existing_type=sa.Numeric(10, 2),
        type_=sa.Float(),
    )

    op.alter_column(
        'vehicles',
        'acquisition_cost',
        existing_type=sa.Numeric(10, 2),
        type_=sa.Float(),
    )
