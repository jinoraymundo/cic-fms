"""empty message

Revision ID: ca31b7295e01
Revises: b6530ec95b17
Create Date: 2017-04-06 00:43:46.641384

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ca31b7295e01'
down_revision = 'b6530ec95b17'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('employees', sa.Column('remarks', sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('employees', 'remarks')
    # ### end Alembic commands ###
