"""added is_claimable in Incident

Revision ID: 72148ef2eb09
Revises: 337831ae00cb
Create Date: 2017-05-02 16:26:18.577640

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '72148ef2eb09'
down_revision = '337831ae00cb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('incidents', sa.Column('is_claimable', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('incidents', 'is_claimable')
    # ### end Alembic commands ###
