"""modify character limit for LAN

Revision ID: a88cef94f41a
Revises: ac028c37d35c
Create Date: 2017-06-08 15:07:27.746073

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a88cef94f41a'
down_revision = 'ac028c37d35c'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('leasing_information', 'lease_account_no',
                    existing_type=sa.String(14),
                    type_=sa.String(100))


def downgrade():
    op.alter_column('leasing_information', 'lease_account_no',
                    existing_type=sa.String(100),
                    type_=sa.String(14))
