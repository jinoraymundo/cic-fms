"""change LeasingInformation.monthly_rental type from Float to Numeric

Revision ID: c552c7baddc4
Revises: 55d4c2e408d1
Create Date: 2017-05-01 16:34:45.446162

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c552c7baddc4'
down_revision = '55d4c2e408d1'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        'leasing_information',
        'monthly_rental',
        existing_type=sa.Float(),
        type_=sa.Numeric(10, 2),
    )


def downgrade():
    op.alter_column(
        'leasing_information',
        'monthly_rental',
        existing_type=sa.Numeric(10, 2),
        type_=sa.Float()
    )
