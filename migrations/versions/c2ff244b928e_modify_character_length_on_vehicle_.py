"""modify character length on Vehicle.shell_fleet_card_no

Revision ID: c2ff244b928e
Revises: 6d94e5bf1378
Create Date: 2017-04-09 22:56:08.303388

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c2ff244b928e'
down_revision = '6d94e5bf1378'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('vehicles', 'shell_fleet_card',
                    existing_type=sa.String(19),
                    type_=sa.String(25))


def downgrade():
    op.alter_column('vehicles', 'shell_fleet_card',
                    existing_type=sa.String(25),
                    type_=sa.String(19))
