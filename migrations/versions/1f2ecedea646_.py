"""empty message

Revision ID: 1f2ecedea646
Revises: 358c733b193d
Create Date: 2018-01-10 16:17:00.846231

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1f2ecedea646'
down_revision = '358c733b193d'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('insurance', sa.Column('effectivity_period_end', sa.Date(), nullable=True))
    op.add_column('insurance', sa.Column('effectivity_period_start', sa.Date(), nullable=True))
    op.add_column('insurance', sa.Column('oac_policy_no', sa.String(length=50), nullable=True))


def downgrade():
    op.drop_column('insurance', 'oac_policy_no')
    op.drop_column('insurance', 'effectivity_period_start')
    op.drop_column('insurance', 'effectivity_period_end')
