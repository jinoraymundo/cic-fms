"""added classification to Employee

Revision ID: 7602f1957c28
Revises: 0e7d0b17e2bb
Create Date: 2017-04-07 14:07:31.948411

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7602f1957c28'
down_revision = '0e7d0b17e2bb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('employee_classifications_other',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.add_column(u'employees', sa.Column('classification_code_id', sa.Integer(), nullable=True))
    op.drop_constraint(u'employees_classification_id_fkey', 'employees', type_='foreignkey')
    op.create_foreign_key(None, 'employees', 'employee_classifications', ['classification_code_id'], ['id'], ondelete='SET NULL')
    op.create_foreign_key(None, 'employees', 'employee_classifications_other', ['classification_id'], ['id'], ondelete='SET NULL')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'employees', type_='foreignkey')
    op.drop_constraint(None, 'employees', type_='foreignkey')
    op.create_foreign_key(u'employees_classification_id_fkey', 'employees', 'employee_classifications', ['classification_id'], ['id'], ondelete=u'SET NULL')
    op.drop_column(u'employees', 'classification_code_id')
    op.drop_table('employee_classifications_other')
    # ### end Alembic commands ###
