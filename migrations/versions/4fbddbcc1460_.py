"""empty message

Revision ID: 4fbddbcc1460
Revises: 1f2ecedea646
Create Date: 2018-01-10 16:19:20.814102

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4fbddbcc1460'
down_revision = '1f2ecedea646'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('vehicles', 'effectivity_period_start')
    op.drop_column('vehicles', 'oac_policy_no')
    op.drop_column('vehicles', 'effectivity_period_end')


def downgrade():
    op.add_column('vehicles', sa.Column('effectivity_period_end', sa.DATE(), autoincrement=False, nullable=True))
    op.add_column('vehicles', sa.Column('oac_policy_no', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
    op.add_column('vehicles', sa.Column('effectivity_period_start', sa.DATE(), autoincrement=False, nullable=True))
